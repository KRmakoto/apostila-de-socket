\select@language {brazil}
\contentsline {chapter}{PREF\IeC {\'A}CIO}{i}{chapter*.2}
\contentsline {chapter}{\numberline {1}INTRODU\IeC {\c C}\IeC {\~A}O}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Sistema Operacional}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Hist\IeC {\'o}ria do Linux}{2}{subsection.1.1.1}
\contentsline {chapter}{\numberline {2}TECNOLOGIA DE SOCKET}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Apresenta\IeC {\c c}\IeC {\~a}o das fun\IeC {\c c}\IeC {\~o}es}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Fun\IeC {\c c}\IeC {\~a}o socket()}{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Fun\IeC {\c c}\IeC {\~a}o bind()}{8}{subsection.2.1.2}
\contentsline {paragraph}{O Retorno}{10}{section*.6}
\contentsline {subsection}{\numberline {2.1.3}Fun\IeC {\c c}\IeC {\~a}o listen()}{10}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Fun\IeC {\c c}\IeC {\~a}o accept()}{11}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Fun\IeC {\c c}\IeC {\~a}o connect()}{13}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Fun\IeC {\c c}\IeC {\~a}o recv() \& send() : Transmiss\IeC {\~a}o em soquetes com conex\IeC {\~a}o}{14}{subsection.2.1.6}
\contentsline {subsubsection}{Algumas FLAGS}{16}{section*.7}
\contentsline {subsubsection}{Retornos de send() e recv()}{17}{section*.8}
\contentsline {chapter}{\numberline {3}APLICA\IeC {\c C}\IeC {\~A}O DO SOCKET}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Lado Servidor}{20}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Cabe\IeC {\c c}alhos utilizados}{20}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Defini\IeC {\c c}\IeC {\~o}es e macros}{21}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Vari\IeC {\'a}veis atribu\IeC {\'\i }das}{21}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Inicializa\IeC {\c c}\IeC {\~a}o das vari\IeC {\'a}veis}{23}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Aplica\IeC {\c c}\IeC {\~a}o da fun\IeC {\c c}\IeC {\~a}o socket()}{23}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Aplica\IeC {\c c}\IeC {\~a}o da fun\IeC {\c c}\IeC {\~a}o bind()}{24}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}Aplica\IeC {\c c}\IeC {\~a}o da fun\IeC {\c c}\IeC {\~a}o listen()}{25}{subsection.3.1.7}
\contentsline {subsection}{\numberline {3.1.8}Aplica\IeC {\c c}\IeC {\~a}o das fun\IeC {\c c}\IeC {\~o}es accept() e send()}{25}{subsection.3.1.8}
\contentsline {section}{\numberline {3.2}Lado Cliente}{27}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Vari\IeC {\'a}veis - CLIENTE}{28}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Inicializa\IeC {\c c}\IeC {\~a}o das vari\IeC {\'a}veis - CLIENTE}{28}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Fun\IeC {\c c}\IeC {\~a}o socket() - CLIENTE}{29}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Fun\IeC {\c c}\IeC {\~a}o connect() - CLIENTE}{29}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Fun\IeC {\c c}\IeC {\~a}o recv() - CLIENTE}{30}{subsection.3.2.5}
\contentsline {chapter}{\numberline {4}AN\IeC {\'A}LISE DA FUNCIONAMENTO DO CLIENTE E SERVIDOR}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}Portas abertas ou fechadas?}{31}{section.4.1}
\contentsline {section}{\numberline {4.2}An\IeC {\'a}lise do tr\IeC {\'a}fego TCP}{33}{section.4.2}
\contentsline {chapter}{\numberline {5}Anexo}{37}{chapter.5}
\contentsline {section}{\numberline {5.1}C\IeC {\'o}digo Cliente TCP - sockaddr\_in}{37}{section.5.1}
\contentsline {section}{\numberline {5.2}C\IeC {\'o}digo Servidor TCP - sockaddr\_in}{40}{section.5.2}
