\TOCentrysubsec{1}{Using Malvern G}{1}
\TOCentrysubsec{2}{Macro file}{1}
\TOCentrysubsec{3}{The alphabet}{1}
\TOCentrysubsec{4}{Composite letters}{2}
\TOCentrysubsec{5}{Punctuation}{2}
\TOCentrysubsec{6}{One-accent Greek}{2}
\TOCentrysubsec{7}{Variant glyphs}{3}
\TOCentrysubsec{8}{References}{3}
\TOCentrysubsec{9}{Examples of text in Malvern 55 and 56}{4}
