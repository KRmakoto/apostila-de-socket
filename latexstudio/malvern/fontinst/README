
		     Fontinst files for Malvern 1.2
			     pdc 1994.07.20

Malvern is a sanserif METAFONT font family, which should be available
from the CTAN archives (such as ftp.tex.ac.uk in directory tex-archive)
in directory fonts/malvern.

These files are intended to be used with Alan Jeffrey's Fontinst
package.  Fontinst is a package for generating composite ("virtual")
fonts, written in TeX for portability.  For example, you can use
Fontinst to create Malvern fonts with the new Cork (T1) encoding.  You
can get fontinst from the CTAN, in directory fonts/utilities/fontinst.

Encoding files
    These can be used inside an fontinst file, and can also be run
    through plain TeX to produce a printed copy of the encoding.

    pdcmaa.etx -- describes MAlvern A encoding

    pdcmaa3.etx -- describes MAlvern A encoding with two changes: (1) no
	old-style figures (2) no medium capitals

    pdcmab.etx -- describes MAlvern B encoding

Glyph fudge files
    Definitions used to assemble glyphs in Xx into glyphs for Yy are
    called pdcxxyy.mtx (think "Xx to Yy")

    pdcmat1.mtx -- glyph hacks for making T1 (Cork) glyphs from MAlvern
	encoded fonts.  This goes *before* latin.mtx.

    pdcadma.mtx -- glyph hacks for making MAlvern glyphs from ADobe
	standard roman glyphs

Macro files
    pdcetxf.tex
	-- ETX Formatting macros (used when running TeX on an ETX file to
	make a printed copy of the character encoding)

    pdcetxm.tex
	-- ETX Macros (used in the business part of an ETX file)

Miscellaneous 
    example.tex -- an example TeX file that generates fmvmq10.vpl
------------------------------------------------------------------------
