% pdcmaa.etx 1.1.1 1994/07/20 -- Malvern encoding A -*-TeX-*-
%%%========================================================================
%%% @TeX-font-encoding-file{
%%%    author              = "Damian Cugley",
%%%    version             = "1.1",
%%%    date                = "1994/07/20",
%%%    filename            = "pdcmaa.etx",
%%%    address             = "Oxford University Computing Laboratory,
%%%                           Parks Road, Oxford  OX1 3QD, UK",
%%%    telephone           = "+44 865 273838 x 73199",
%%%    email               = "Damian.Cugley@comlab.ox.ac.uk",
%%%    codetable           = "USASCII",
%%%    keywords            = "encoding, Malvern, TeX",
%%%    supported           = "no",
%%%    abstract            = "This is the Malvern A encoding as a TeX font 
%%%                           encoding file, for use with the fontinst 
%%%                           font installation package.",
%%%    package             = "Malvern",
%%%    dependencies        = "fontinst.sty, pdcetxf.tex, pdcetxm.tex",
%%% }
%%%========================================================================
%
% This file is based on the Malvern A encoding used with my Malvern fonts.
% Created pdc 1993/10/01

\relax

\catcode`@=11 \input fontdoc.sty \catcode`@=12
\input pdcetxf

%  Following 4 lines get the version ID from SCCS:
\begingroup\catcode`\%=12 \toks0={\endgroup
	\def\pdcversion{1.1} 
	\def\lastedit{1994/07/20}
}\the\toks0

\title{The Malvern~A encoding~vector}

\section{Introduction}
	This document describes my Malvern~A \TeX\ font encoding
	conventions.  It is in Alan Jeffrey's Fontinst format, which
	means that it can be used to generate {\tt vpl} files remapping
	Malvern-encoded fonts to T1 (Cork) fonts and vice versa.

	The short code for this encoding (for the purposes of Fontinst
	and NFSS~2\footnote*{The second New Font Selection Scheme -- the font
	selection scheme that is used in \LaTeX~2e.}) is `{\tt
	maa}'.\footnote\dag{{\it Ma} is for the Malvern package.  In
	Karl Berry's font naming scheme, Malvern is abbreviated {\it
	Mv}.  But in all other contexts, I have abbreviated it {\it
	Ma}.} The encoding file is called {\tt pdcmaa.etx}, the `pdc'
	prefix intended to reduce the probabilty of my font encoding
	files clashing with anyone else's.

	Malvern~A has the following variants (which only exist because
	most available PostScript fonts have neither medium capital nor
	small capital letters):
\smallskip \item{$\bullet$}
	A.1\quad Medium capital letters replaced by the corresponding full
	capitals.

\smallskip\item{$\bullet$}
	A.2\quad Old style numerals replaced by ranging numerals.

\smallskip\item{$\bullet$}
	A.3\quad Medium capital letters replaced by the corresponding full
	capitals and old style numerals replaced by ranging numerals.
\smallbreak\noindent
	Which encoding is produced by using this {\tt.etx} file depends
	on the values of integer variables {\tt nomedium} and {\tt
	nooldstyle}.  If set to any value these variables suppress
	medium capitals and old style numerals respectively.

\iffalse
	There are at least the following Malvern encodings:
\list
\item
	Malvern A (upper and lower case, small caps and medium caps),
\item
	Malvern B (misc symbols, and overflow from A),
\item
	Malvern C (Cyrillic) [partially completed],
\item
	Malvern D (composites), and 
\item
	Malvern G (Greek).
\endlist
\fi	

\section{Terminology}
	I will use {\it composite letters} to refer to those glyphs
	written by combining a {\it base letter} and some sort of {\it
	mark}, for example, {\it\'e}, {\it\^\j} or {\it\c t}.  I avoid
	the term `accented letter' because it tends to provoke irate
	email informing me that letters like {\it \"a} and {\it \aa} are
	not `accented letters' in the languages where they are used.

	The word {\it composite} refers to the way the glyph is written,
	and does not imply that they will be typeset by overprinting or
	anything similar.

\section{Medium capitals}
	The medium-capital alphabet is a peculiarity of the Malvern
	encoding but is not entirely without precedent (there are other
	typefaces that supply a smaller set of capitals).  M.cap.\
	letters might be used with the `upper case marks' to make
	composite letters that do not poke above the line of text as
	much as plain \TeX's upper case composites do.  Some computer
	documentation uses medium capitals for all-capital acronyms.
	Some people have suggested using medium-capitals for languages
	like German which use many initial capitals.

	If a typeface has no medium capitals, then large capitals should
	be substituted (encoding A.1).
\bigskip \hrule \medskip

\encoding

\needsfontinstversion{1.315}
\input pdcetxm
	
\comment{\section{Default values}}
\setstr{codingscheme}{Malvern A}

\setint{italicslant}{0}

\ifisglyph{space}\then
   \setint{interword}{\width{space}}
\else
   \setint{interword}{333}
\fi

\ifisglyph{x}\then
   \setint{xheight}{\height{x}}
\else
   \setint{xheight}{500}
\fi

\comment{\section{Default font dimensions}}
\setint{fontdimen(1)}{\int{italicslant}}              % italic slant
\setint{fontdimen(2)}{\int{interword}}                % interword space
\ifisint{monowidth}\then
   \setint{fontdimen(3)}{0}                           % interword stretch
   \setint{fontdimen(4)}{0}                           % interword shrink
\else
   \setint{fontdimen(3)}{\scale{\int{interword}}{333}}% interword stretch
   \setint{fontdimen(4)}{\scale{\int{interword}}{333}}% interword shrink
\fi   
\setint{fontdimen(5)}{\int{xheight}}                  % x-height
\setint{fontdimen(6)}{1000}                           % quad
\ifisint{monowidth}\then
   \setint{fontdimen(7)}{\int{interword}}             % extra space after .
\else
   \setint{fontdimen(7)}{0}                           % extra space after .
\fi

\comment{\section{The encoding}
   There are 256 glyphs in this encoding.}

\setslot{Thorn}
   \comment{The Icelandic capital letter Thorn, similar to a `P' with the 
      bowl moved down.  It is unavailable in plain \TeX.}
\endsetslot

\setslot{Aogonek}
   \comment{The letter `A' with an ogonek accent hanging from the bottom 
      right serif.}
\endsetslot

\setslot{Ng}
   \comment{The Sami letter `Ng', which looks like an uncial `N' (or a 
      large `n') with a right tail taken from a `j'.  It is unavailable 
      in plain \TeX.}
\endsetslot

\setslot{Lslash}
  \comment{The uppercase Polish letter `\L'}
\endsetslot

\setslot{Eth}
   \comment{The uppercase Icelandic letter `Eth' similar to a `D' with a 
      horizontal bar through the stem.  It is unavailable in plain \TeX.}
\endsetslot

\setslot{Eogonek}
   \comment{The letter `E' with an ogonek hanging from the right of the 
      bottom bar.}
\endsetslot

\setslot{cedilla}
   \comment{The cedilla mark `\c c'.}
\endsetslot

\setslot{ogonek}
   \comment{The ogonek mark (unavailable in plain \TeX).}
\endsetslot

\setslot{acute}
   \comment{The acute mark `\'a'.}
\endsetslot

\setslot{grave}
   \comment{The grave mark `\`a'.}
\endsetslot

\setslot{circumflex}
   \comment{The circumflex mark `\^ a'.}
\endsetslot

\setslot{tilde}
   \comment{The tilde mark `\~a'.}
\endsetslot

\setslot{ring}
   \comment{The ring mark `\aa'.}
\endsetslot

\setslot{caron}
   \comment{The hook (h\'a\v cek) mark `\v c'.}
\endsetslot

\setslot{breve}
   \comment{The breve mark `\u a'.}
\endsetslot

\setslot{hungarumlaut}
   \comment{The long Hungarian umlaut mark `\H a'.}
\endsetslot

\setslot{thorn}
   \comment{The Icelandic lower case letter thorn, similar to a `p' with
	the ascender of a `b'.  It is unavailable in plain \TeX.}
\endsetslot

\setslot{aogonek}
   \comment{The letter `a' with an ogonek mark hanging from the bottom 
      right stroke.}
\endsetslot

\setslot{ng}
   \comment{The Sami letter `eng', which looks like an `n' with a right
	tail like a `j'.  It is unavailable in plain \TeX.}
\endsetslot

\setslot{lslash}
  \comment{The lower case  Polish letter `\l'}
\endsetslot

\setslot{eth}
   \comment{The lower case Icelandic letter `eth' similar to a
	`$\partial$' with a stroke through the ascender.  It is
	unavailable in plain \TeX.}
\endsetslot

\setslot{eogonek}
   \comment{The letter `e' with an ogonek mark hanging from bottom
	stroke.}
\endsetslot

\setslot{dotaccent}
   \comment{The dot mark `\.c'.}
\endsetslot

\setslot{dieresis}
   \comment{The two-dots mark `\"a'.}
\endsetslot

\setslot{germandbls}
   \comment{The German {\it Eszet}\/ `\ss'.}
\endsetslot

\setslot{dotlessi}
   \comment{A dotless {\it i} `\i', used to produce composite letters
	such as `\^\i'.}
\endsetslot

\setslot{dotlessj}
   \comment{A dotless {\it j} `\j', used to produce composite letters
	such as `\^\j' (Esperanto letter).}
\endsetslot

\setslot{ff}
   \ligature{LIG}{i}{ffi}
   \ligature{LIG}{l}{ffl}
   \comment{The `ff' ligature.}
\endsetslot

\setslot{fi}
   \comment{The `fi' ligature.}
\endsetslot

\setslot{fl}
   \comment{The `fl' ligature.}
\endsetslot

\setslot{ffi}
   \comment{The `ffi' ligature.}
\endsetslot

\setslot{ffl}
   \comment{The `ffl' ligature.}
\endsetslot

\setslot{bullet}
  \comment{A bullet `$\bullet$'.}
\endsetslot

\setslot{exclam}
   \ligature{LIG}{quoteleft}{exclamdown}
   \comment{The exclamation mark `!'.}
\endsetslot

\setslot{trademark}
  \comment{The trademark sign, `\trademark'.
	I~deliberately did not put anything resembling a quotation mark
	in this slot, in the hope that that people who think that they
	can use `neutral quotation marks' in typeset text will be
	trained out of this bad habit.}
\endsetslot


\setslot{numbersign}
   \comment{The hash mark `\#'.}
\endsetslot

\setslot{dollar}
   \comment{The dollar sign `\$'.}
\endsetslot

\setslot{percent}
   \comment{The percent sign `\%'.}
\endsetslot

\setslot{ampersand}
   \comment{The ampersand sign `\&'.}
\endsetslot

\setslot{quoteright}
   \ligature{LIG}{quoteright}{quotedblright}
   \comment{The apostrophe `\thinspace'\thinspace'.}
\endsetslot

\setslot{parenleft}
   \comment{The opening parenthesis `('.}
\endsetslot

\setslot{parenright}
   \comment{The closing parenthesis `)'.}
\endsetslot

\setslot{asterisk}
   \comment{The raised asterisk `*'.}
\endsetslot

\setslot{plus}
   \comment{The addition sign `+'.}
\endsetslot

\setslot{comma}
   \ligature{LIG}{comma}{quotedblbase}
   \comment{The comma `,'.}
\endsetslot

\setslot{hyphen}
   \ligature{LIG}{hyphen}{rangedash}
   \comment{The hyphen `-'.}
\endsetslot

\setslot{period}
   \comment{The full point `.'.}
\endsetslot

\setslot{slash}
   \comment{The forward oblique `/'.}
\endsetslot


\ifisint{nooldstyle}\then
    \pdcnumerals{ranging}{}{$#1$}
\else
    \comment{\medskip The old style numerals.  In effect, old style
	figures the default for plain text.  \endgraf By using
	{\tt\char92 mathcode}s, a document designer can arrange that
	ranging numerals be used in maths mode, and then arrange that
	all numbers be set in maths mode.  (It might be easier to use a
	virtual font to exchange the ranging and old style numerals and
	not use old style figures at all.)  \endgraf This and the
	following nine glyphs might be the same as glyphs 176--87, if
	the typeface has no old style digits.}

    \pdcnumerals{old style}{oldstyle}{$\mit#1$}
\fi

\setslot{colon}
   \comment{The colon `:'.}
\endsetslot

\setslot{semicolon}
   \comment{The semi-colon `;'.}
\endsetslot

\setslot{guilsinglleft}
   \comment{A leftward-pointing single guillemet (cf.\ glyph~188), not
	available in plain \TeX.}
\endsetslot

\setslot{equal}
   \comment{The equals sign `='.}
\endsetslot

\setslot{guilsinglright}
   \comment{A rightward-pointing single guillemet (cf.\ glyph~190), not
	available in plain \TeX.}
\endsetslot

\setslot{question}
   \ligature{LIG}{quoteleft}{questiondown}
   \comment{The question mark `?'.}
\endsetslot

\setslot{at}
   \comment{The commerical at sign `@'.}
\endsetslot


\pdcuppercase{capital}{}{\uppercase{#1}}


\setslot{bracketleft}
   \comment{The opening square bracket `['.}
\endsetslot

\setslot{Oslash}
   \comment{The letter `\O'.}
\endsetslot

\setslot{bracketright}
   \comment{The closing square bracket `]'.}
\endsetslot

\setslot{AE}
   \comment{The letter `\AE'.}
\endsetslot

\setslot{OE}
   \comment{The letter `\OE'.}
\endsetslot

\setslot{quoteleft}
   \ligature{LIG}{quoteleft}{quotedblleft}
   \comment{The inverted (turned) comma `\thinspace`\thinspace'.  English opening
	quotation mark, German closing nested quotation mark.}
\endsetslot

\pdclowercase{lower case}{}{\lowercase{#1}}

\setslot{braceleft}
   \comment{The opening curly brace `$\lbrace$'.}
\endsetslot

\setslot{oslash}
   \comment{The lower case letter `\o'.}
\endsetslot
 
\setslot{braceright}
   \comment{The closing curly brace `$\rbrace$'.}
\endsetslot

\setslot{ae}
   \comment{The lower case letter `\ae'.}
\endsetslot

\setslot{oe}
   \comment{The lower case letter `\oe'.}
\endsetslot

\ifisint{nomedium}\then

\setslot{Thorn}
   \comment{The capital Icelandic letter Thorn, same as glyph~0.}
\endsetslot

\setslot{Aogonek}
   \comment{The  capital letter `A' with an ogonek hanging from
	the bottom right serif, same as glyph~1}
\endsetslot

\setslot{Ng}
   \comment{The  capital Sami letter `Eng', same as glyph~2.}
\endsetslot

\setslot{Lslash}
  \comment{The  capital Polish letter `\L', same as glyph~3.}
\endsetslot

\setslot{Eth}
   \comment{The  capital Icelandic letter `Eth', same as glyph~4.}
\endsetslot

\setslot{Eogonek}
   \comment{The capital letter `E' with an ogonek hanging from
	the right of the bottom bar, same as glyph~5.}
\endsetslot

\else

\setslot{Thornmedium}
   \comment{The medium capital Icelandic letter Thorn, similar to a `P'
	with the bowl moved down.  It is unavailable in plain \TeX.  See
	\S\thinspace XXX for information about medium capital letters.}
\endsetslot

\setslot{Aogonekmedium}
   \comment{The medium capital letter `A' with an ogonek hanging from
	the bottom right serif.  The PostScript conventions for naming
	glyphs begin to fray at this point\dots}
\endsetslot

\setslot{Ngmedium}
   \comment{The medium capital Sami letter `Eng', which looks like an
	uncial `N' (or a large `n') with a right tail taken from a `j'.
	It is unavailable in plain \TeX.}
\endsetslot

\setslot{Lslashmedium}
  \comment{The medium capital Polish letter `\L'}
\endsetslot

\setslot{Ethmedium}
   \comment{The medium capital Icelandic letter `Eth' similar to a `D'
	with a horizontal bar through the stem.  It is unavailable in
	plain \TeX.}
\endsetslot

\setslot{Eogonekmedium}
   \comment{The medium capital letter `E' with an ogonek hanging from
	the right of the bottom bar.}
\endsetslot
\fi

\setslot{cedillacap}
   \comment{The cedilla mark `\c C', sized for capital letters.}
\endsetslot

\setslot{ogonekcap}
   \comment{The ogonek mark, sized for capital leters (unavailable in
	plain \TeX).}
\endsetslot

\setslot{acutecap}
   \comment{The acute mark `\'A', sized for (medium) capital letters.
	For all these `capital marks', the glyph is positioned
	vertically in the correct position to go over a lower case
	letter, even though it is intended to be used over capital or
	medioum capital base glyphs.  This is so that \TeX's
	{\tt\char92 accent} primitive may be used to position the
	mark.}
\endsetslot

\setslot{gravecap}
   \comment{The grave mark `\`A', sized for capital letters.}
\endsetslot

\setslot{circumflexcap}
   \comment{The circumflex mark `\^ A', sized for capital letters.}
\endsetslot

\setslot{tildecap}
   \comment{The tilde mark `\~A', sized for capital letters.}
\endsetslot

\setslot{ringcap}
   \comment{The ring mark `\AA', sized for capital letters.}
\endsetslot

\setslot{caroncap}
   \comment{The hook (h\'a\v cek) mark `\v C', sized for
	capital letters.}
\endsetslot

\setslot{brevecap}
   \comment{The breve mark `\u A', sized for capital letters.}
\endsetslot

\setslot{hungarumlautcap}
   \comment{The long Hungarian umlaut mark `\H A', sized for 
	capital letters.}
\endsetslot

\setslot{Thornsmall}
   \comment{The Icelandic small capital letter thorn, similar to a `{\sc
	p}', but with the bowl lowered.  It is unavailable in plain
	\TeX.}
\endsetslot

\setslot{Aogoneksmall}
   \comment{The small capital letter `{\sc a}' with an ogonek mark
	hanging from the bottom right stroke.}
\endsetslot

\setslot{Ngsmall}
   \comment{The Sami small capital letter `eng'.  It is unavailable in
	plain \TeX.}
\endsetslot

\setslot{Lslashsmall}
  \comment{The small capital  Polish letter `{\sc\l}'}
\endsetslot

\setslot{Ethsmall}
   \comment{The small capital Icelandic letter `eth' similar to a `{\sc
	d}' with a stroke through the left stem.  It is unavailable in
	plain \TeX.}
\endsetslot

\setslot{Eogoneksmall}
   \comment{The small capital letter `{\sc e}' with an ogonek mark
	hanging from bottom bar.}
\endsetslot

\setslot{macron}
   \comment{The macron mark `\=a'.}
\endsetslot

\setslot{macrondbl}
   \comment{The macron mark `$\overline{\rm aa}$', intended to be wide
	enough for two letters.}
\endsetslot

\setslot{ordfeminine}
    \comment{A superior (raised) lower case letter {\it a}, optionally
	with a bar under: `\flushtop{\the\scriptfont\fam a}' or
	`\flushtop{\the\scriptfont\fam \b{a}}'.}
\endsetslot

\setslot{ordmasculine}
    \comment{A superior lower case letter {\it o}, optionally with a bar
	under: `\flushtop{\the\scriptfont\fam o}' or `\flushtop{\the\scriptfont\fam
	\b{o}}'.  Thus `N\flushtop{\the\scriptfont\fam \b{o}}'.}
\endsetslot

\setslot{careof}
    \comment{A superior {\it c} and infoerior {\it o} separated by a
	slash.  This, or `c/o', is sometimes used to abbrevate `care
	of'.}
\endsetslot

\setslot{csuperior1}
    \comment{A superior lower case letter {\it c}, optionally with a bar
	under: `\flushtop{\the\scriptfont\fam c}' or `\flushtop{\the\scriptfont\fam
	\b{c}}'.  The `-1' in the name indicates it is a
	variation on plain `csuperior', which would presumably  lack the
	bar.  Thus `M\flushtop{\the\scriptfont\fam \b{c}}Donald'.}
\endsetslot


\setslot{dcaron}
   \comment{The lower case letter `d' with a hook mark, which is usually
	drawn as a curl from the right of the ascender rather than a
	`\v{}' over the letter.}
\endsetslot

\setslot{tcaron}
   \comment{The lower case letter `t' with a hook mark, which is usually
	drawn as a curl from the right of the ascender rather than a
	`\v{}' over the letter.}
\endsetslot

\setslot{hcircumflex}
   \comment{The lower case letter `h' with a circumflex mark (the
	Esperanto letter hho).  Although usually drawn with the mark
	above the ascender, I would suggest that the same considerations
	as `dcaron' and `tcaron' apply, and the mark might better be
	drawn to the right of the ascender.}
\endsetslot

\setslot{lcaron}
   \comment{The lower case letter `L' with a hook mark, which is usually
	drawn as a curl from the right of the ascender rather than a
	`\v{}' over the letter.}
\endsetslot


\setslot{ballotbox}
   \comment{A ballot-box (hollow square).}
\endsetslot

\setslot{exclamdown}
   \comment{The inverted exclamation mark `!`'.}
\endsetslot

\setslot{cent}
    \comment{A cents sign: c or \rlap{/}c.}
\endsetslot

\setslot{sterling}
   \comment{The British currency mark `\pounds'.}
\endsetslot

\setslot{currency}
   \comment{The ISO 646 currency mark, a ring with four ears pointing
	NW, NE, SW and SE.}
\endsetslot

\setslot{perthousand}
    \comment{The per-thousand sign, like a `\%' but with two rings below
	the slash.}
\endsetslot

\setslot{multiply}
    \comment{The multiplication sign $\times$.}
\endsetslot

\setslot{quotedblright}
   \comment{A double apostrophe `\thinspace''\thinspace', a English closing quotation
	mark.}
\endsetslot

\setslot{yen}
    \comment{The Japanese yen currency sign
	{\setbox0=\hbox{Y}\rlap{\hbox to \wd0 {\hfil \the\scriptfont\fam=\hfil
	}}\box0 }.}
\endsetslot

\setslot{florin}
    \comment{The florin currency sign, an italic `{\it f\/}'.}
\endsetslot

\setslot{dagger}
    \comment{A dagger sign `\dag'.}
\endsetslot

\setslot{daggerdbl}
    \comment{A double dagger sign `\ddag'.}
\endsetslot

\setslot{quotedblbase}
   \comment{A double comma `,\kern-1pt,' used as an opening quotation
	mark in German.  Note that I have not included a
	`quotesingbase', which implies that a comma must be used
	instead.  Sorry.}
\endsetslot

\setslot{rangedash}
   \ligature{LIG}{hyphen}{punctdash}
   \comment{The en-dash, used in English as a number range dash `1--9'.}
\endsetslot

\setslot{periodcentered}
    \comment{A raised full stop, used as a decimal point in English
	typesetting: `$3{\cdot}142$' rather than `3,142'.}
\endsetslot

\setslot{minus}
    \comment{A minus sign, `$-$', so that it is possible to typeset
	`$-1\,^\circ\rm C$' without using symbol fonts.}
\endsetslot

\pdcnumerals{ranging}{}{$#1$}

\setslot{section}
   \comment{The section sign `\S'.}
\endsetslot

\setslot{paragraph}
   \comment{The paragraph sign or pilcrow `\P'.}
\endsetslot

\setslot{guillemotleft}
   \comment{A leftward-pointing guillemet, unavailable in plain \TeX\
	but not entirely unlike `\lguil'.  French opening quote mark,
	German closing quote mark.}
\endsetslot

\setslot{punctdash}
   \comment{The em-dash, or punctuation dash `Oh---boy'.}
\endsetslot

\setslot{guillemotright}
   \comment{A rightward-pointing guillemet, unavailable in plain \TeX\
	but not entirely unlike `\rguil'.  French closing quote mark,
	German opening quote mark.}
\endsetslot

\setslot{questiondown}
   \comment{The inverted question mark `?`'.}
\endsetslot

\setslot{degree}
    \comment{The degree sign `$^\circ$'.}
\endsetslot

\ifisint{nomedium}\then
    \pdcuppercase{duplicate capital}{}{\uppercase{#1}}
\else
    \pdcuppercase{medium capital}{medium}{\uppercase{#1}}
\fi

\setslot{angleleft}
    \comment{An opening angle bracket `$\langle$'.}
\endsetslot

\ifisint{nomedium}\then
\setslot{Oslash}
   \comment{Duplicate capital `\O'.}
\endsetslot
\else
\setslot{Oslashmedium}
   \comment{The medium capital `\O'.}
\endsetslot
\fi

\setslot{angleright}
    \comment{An closing angle bracket `$\rangle$'.}
\endsetslot

\ifisint{nomedium}\then
\setslot{AE}
   \comment{The capital letter `\AE'.}
\endsetslot

\setslot{OE}
   \comment{The capital letter `\OE'.}
\endsetslot
\else
\setslot{AEmedium}
   \comment{The  medium capital letter `\AE'.}
\endsetslot

\setslot{OEmedium}
   \comment{The medium capital letter `\OE'.}
\endsetslot
\fi

\setslot{quotedblleft}
   \comment{A double inverted comma, `\thinspace``\thinspace'.  
	An English opening
	quotation mark and German closing quotation mark.}
\endsetslot

\pdcuppercase{small capital}{small}{{\sc \lowercase{#1}}}

\setslot{copyright}
    \comment{A small capital `{\sc c}' in a circle: `\copyright', the
	pan-galactic symbol for `copyright'.}
\endsetslot


\setslot{Oslashsmall}
   \comment{The small capital `{\sc\o}'.}
\endsetslot

\setslot{registered}
    \comment{A small capital `{\sc r}' in a circle, standing for
	`registered trade mark': `\registered'.}
\endsetslot

\setslot{AEsmall}
   \comment{The  small capital letter `{\sc\ae}'.}
\endsetslot

\setslot{OEsmall}
   \comment{The small capital letter `{\sc\oe}'.}
\endsetslot


\endencoding

\leftline{(End of encoding.)}

\bye

%Local Variables:
%tex-has-children: t
%fill-prefix: "	"
%End:
