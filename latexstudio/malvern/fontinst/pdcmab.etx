% pdcmab.etx 1.1.1 1994/07/20 -- Malvern encoding B -*-TeX-*-
%%%========================================================================
%%% @TeX-font-encoding-file{
%%%    author              = "Damian Cugley",
%%%    version             = "1.1",
%%%    date                = "1994/07/20",
%%%    filename            = "pdcmab.etx",
%%%    address             = "Oxford University Computing Laboratory
%%%                           Parks Road
%%%                           Oxford  OX1 3QD
%%%                           UK",
%%%    telephone           = "+44 865 273838 x 73199",
%%%    email               = "Damian.Cugley@comlab.ox.ac.uk",
%%%    codetable           = "ISO/ASCII",
%%%    keywords            = "encoding, Malvern, TeX, PostScript",
%%%    supported           = "no",
%%%    abstract            = "This is the Malvern B encoding as a TeX font 
%%%                           encoding file, for use with the fontinst 
%%%                           font installation package.",
%%%    package             = "Malvern",
%%%    dependencies        = "fontinst.sty, fontdoc.sty, 
%%%				pdcetxf.tex, pdcetxm.tex",
%%% }
%%%========================================================================
%
% This file is based on the Malvern B encoding used with my Malvern fonts.
% Created pdc 1993/10/01

\relax

\catcode`@=11 \input fontdoc.sty \catcode`@=12
\input pdcetxf

%  Following 4 lines get the version ID from SCCS:
\begingroup\catcode`\%=12 \toks0={\endgroup
	\def\pdcversion{1.1} 
	\def\lastedit{1994/07/20}
}\the\toks0

\title{The Malvern~B (symbol) encoding vector}
\section{Introduction}
	This document describes my Malvern~B \TeX\ font encoding
	conventions.  It is in Alan Jeffrey's {\tt fontinst} format,
	which means that it can be used to generate {\tt vpl} files
	remapping Malvern-encoded fonts to T1 (Cork) fonts and vice
	versa.

	The short code for this encoding (for the purposes of {\tt
	fontinst} and NFSS~2\footnote*{The second New Font Selection
	Scheme, used in \LaTeX~2e.}) is `{\tt mab}'.\footnote\dag{{\it
	Ma} is for the Malvern package.  In Karl Berry's font naming
	scheme, Malvern is abbreviated {\it Mv}.  But in all other
	contexts, I have abbreviated it {\it Ma}.}  Thus the encoding
	file is called {\tt pdcmab.etx}, the `pdc' prefix intended to
	reduce the probabilty of my font encoding files clashing with
	anyone else's.

	The B encoding consists mainly of miscellaneous symbols that
	don't fit into Malvern~A.  It includes those needed to generate
	Cork-encoded fonts with Malvern~A, and a fair number of special
	letters.
\encoding

\needsfontinstversion{1.315}

\comment{\section{Default values}}

\setstr{codingscheme}{Malvern B}

\setint{italicslant}{0}

\ifisglyph{space}\then
   \setint{interword}{\width{space}}
\else
   \setint{interword}{333}
\fi

\ifisglyph{x}\then
   \setint{xheight}{\height{x}}
\else
   \setint{xheight}{500}
\fi

\comment{\section{Default font dimensions}}

\setint{fontdimen(1)}{\int{italicslant}}              % italic slant
\setint{fontdimen(2)}{\int{interword}}                % interword space
\ifisint{monowidth}\then
   \setint{fontdimen(3)}{0}                           % interword stretch
   \setint{fontdimen(4)}{0}                           % interword shrink
\else
   \setint{fontdimen(3)}{\scale{\int{interword}}{333}}% interword stretch
   \setint{fontdimen(4)}{\scale{\int{interword}}{333}}% interword shrink
\fi   
\setint{fontdimen(5)}{\int{xheight}}                  % x-height
\setint{fontdimen(6)}{1000}                           % quad
\ifisint{monowidth}\then
   \setint{fontdimen(7)}{\int{interword}}             % extra space after .
\else
   \setint{fontdimen(7)}{0}                           % extra space after .
\fi

\comment{\section{The encoding proper}}

\nextslot{0}
\setslot{arrowleft}
    \comment{An arrow pointing left, `$\leftarrow$'.}
\endsetslot

\setslot{arrowright}
    \comment{An arrow pointing right, `$\rightarrow$'.}
\endsetslot

\setslot{arrowup}
    \comment{An arrow pointing up, `$\uparrow$'.}
\endsetslot

\setslot{arrowdown}
    \comment{An arrow pointing down, `$\downarrow$'.}
\endsetslot

\setslot{arrowboth}
    \comment{An arrow pointing left and right, `$\leftrightarrow$'.}
\endsetslot

\setslot{arrowupdown}
    \comment{An arrow pointing up and down, `$\updownarrow$'.}
\endsetslot

\setslot{arrowhorizex}
    \comment{A rule that can be used to extend horizontal arrows (to
	make `$\longrightarrow$' etc.).}
\endsetslot

\setslot{arrowvertex}
    \comment{A rule that can be used to extend vertical arrows.}
\endsetslot

\nextslot{32}
\setslot{space}
    \comment{A blank space `~', with width of a normal interword space.
	Cf.\thinspace glyph~160.}
\endsetslot

\nextslot{`\"}
\setslot{quotedbl}
    \comment{A doubled tick mark, `\tickdbl' or `{\tt"}' (cf.\
	glyph~39).  This is the \tickdbl neutral double quotation
	mark\tickdbl\ which will be used when setting computer
	langauges, or to simulate the output of DTP programs.}
\endsetslot

\nextslot{`\!}
\setslot{brokenbar}
    \comment{A broken vertical bar.}
\endsetslot

\nextslot{`\%}
\setslot{perthousandzero}
    \comment{A T1 glyph that when jutxaposed with a per cent character
	produces a per-mille.}
\endsetslot

\nextslot{`\'}
\setslot{quotesingle}
    \comment{An small tick mark, `\tick' or `{\tt\char13 }'.  The ASCII
	combined \tick single quote\tick, apostrophe and acute accent
	all rolled into one.  Use this for LISP's `quote' and as the
	neutral single quotation mark when setting other computer
	languages.  Used in some formatters and in \TeX\ to stand for an
	apostrophe `\thinspace'\thinspace'.}
\endsetslot

\nextslot{`,}
\setslot{quotesinglbase}
    \comment{A comma `,', spaced suitably to be used as a German opening
	quotation mark.}
\endsetslot

\nextslot{`.}
\setslot{ellipsis}
    \comment{An ellipsis `\dots'.}
\endsetslot

\nextslot{`\/}
\setslot{fraction}
    \comment{A slash `/', with negative sidebearings suitable for making
	fractions like `\frac8/9'.}
\endsetslot

\nextslot{`0}
\setslot{zerosuperior}
    \comment{Superior numeral zero, `\flushtop{\the\scriptfont0 0}'.}
\endsetslot

\setslot{onesuperior}
    \comment{Superior numeral one, `\flushtop{\the\scriptfont0 1}'.}
\endsetslot

\setslot{twosuperior}
    \comment{Superior numeral two, `\flushtop{\the\scriptfont0 2}'.}
\endsetslot

\setslot{threesuperior}
    \comment{Superior numeral three, `\flushtop{\the\scriptfont0 3}'.}
\endsetslot

\setslot{foursuperior}
    \comment{Superior numeral four, `\flushtop{\the\scriptfont0 4}'.}
\endsetslot

\setslot{fivesuperior}
    \comment{Superior numeral five, `\flushtop{\the\scriptfont0 5}'.}
\endsetslot

\setslot{sixsuperior}
    \comment{Superior numeral six, `\flushtop{\the\scriptfont0 6}'.}
\endsetslot

\setslot{sevensuperior}
    \comment{Superior numeral seven, `\flushtop{\the\scriptfont0 7}'.}
\endsetslot

\setslot{eightsuperior}
    \comment{Superior numeral eight, `\flushtop{\the\scriptfont0 8}'.}
\endsetslot

\setslot{ninesuperior}
    \comment{Superior numeral nine, `\flushtop{\the\scriptfont0 9}'.}
\endsetslot

\nextslot{`:}
\setslot{divide}
    \comment{A division sign `$\mathchar"204 $'.}
\endsetslot

\nextslot{`\<}
\setslot{less}
    \comment{A less-than sign, `$<$'.}
\endsetslot


\nextslot{`\>}
\setslot{greater}
    \comment{A greater-than sign, `$>$'.}
\endsetslot

\nextslot{`\\}
\setslot{backslash}
    \comment{A reversed solidus, `$\backslash$'.}
\endsetslot

\nextslot{`\^}
\setslot{asciicircum}
    \comment{An ASCII circumflex character,
	`\asciicircum', something
	between `{\tt\^{}}' and `$\wedge$'.  Used as a dereferencing
	operator in Pascal ($x\asciicircum.f$) and as a binary operator
	in other computer languages ($x \mathbin{\asciicircum} y$).}
\endsetslot

\nextslot{`\_}
\setslot{underscore}
    \comment{An ASCII underscore `{\tt\char95 }', as might be used in
	identifiers in computer programs.}
\endsetslot

\nextslot{`\`}
\setslot{backquote}
    \comment{Originally a grave accent, now generally treated as a
	reverse ASCII quote, `{\tt\`{}}' (mirror image of glyph~39).
	This symbol is used in LISP and some other computer languages as
	a symbol in its own right, and in many formatters to stand in
	for an opening quotation mark `\thinspace`\thinspace'.}
\endsetslot

\setslot{thorn1}
	\comment{The style of lower case thorn used for setting Old
	English texts.}
\endsetslot

\setslot{eth1}
	\comment{The style of lower case eth used for setting Old
	English texts.}
\endsetslot

\setslot{wyn}
	\comment{The obselete letter wyn, used for setting Old English
	texts.  It looks a bit like a {\bf thorn1} without the
	ascender.  A sort of precursor to {\it w}.}
\endsetslot

\setslot{yogh}
	\comment{The obselete letter yogh, used for setting Old English
	texts.  It looks a little like a `3' or the curly sort of {\it
	z}.  A sorty of precursor ot {\it gh}.}
\endsetslot

\setslot{yogh1}
	\comment{The obselete letter ???, used for setting Old English
	texts.  A sort of precursor of {\it g}.}
\endsetslot

\setslot{ch}
	\comment{Ligature {\it ch} (common in German founts, especially
	{\it Fraktur} faces.)}
\endsetslot

\setslot{ck}
	\comment{Ligature {\it ck} (common in German founts), especially
	{\it Fraktur} faces.)}
\endsetslot

\setslot{ct}
	\comment{The ligature {\it ct}.  In archaic fonts this has a
	link between the top of the {\it c} and the {\it t}.}
\endsetslot

\setslot{ft}
	\comment{A ligature {\it ft}.  Seen in German fonts.}
\endsetslot

\setslot{ij}
	\comment{The Dutch special letter {\it ij}.  This looks like a
	digraph, but it is a separate character on Dutch keyboards,
	making it something of a special case, like {\it \ae} and {\it
	\oe}.}
\endsetslot

\setslot{ll}
	\comment{Ligature {\it ll}.  Common in German fonts, especially
	{\it Fraktur} fonts.}
\endsetslot

\setslot{longs}
	\comment{A long {\it s}, which looks like $\smallint$ or like an
	{\it f} with the bar omitted.}
\endsetslot

\setslot{longsi}
	\comment{A ligature of long {\it s} with {\it i}
	($\smallint\!\!\imath$), traditional in {\it Fraktur} fonts.}
\endsetslot

\setslot{longslongs}
	\comment{A double long {\it s}
	($\smallint\!\!\smallint$), traditional in {\it Fraktur} fonts.}
\endsetslot

\setslot{longss}
	\comment{A ligature of long {\it s} with final {\it s}
	($\smallint\!\!s$, or {\it \ss}).  This is how German sharp-s
	(\ss) tends to look in roman fonts.}
\endsetslot

\setslot{longst}
	\comment{A ligature of long {\it s} with {\it t}
	($\smallint\!\!t$), by analogy to {\it ft}.}
\endsetslot

\setslot{st}
	\comment{A ligature of (short) {\it s} with {\it t}, possibly
	with a link between the top of the {\it s} and the {\it t} (like
	{\bf ct}.}
\endsetslot

\setslot{longsz}
	\comment{A ligature of long {\it s} with {\it z}
	($\smallint\!\!z$).  In {\it Fraktur} this is how the sharp-s
	(\ss) looks.}
\endsetslot

\setslot{tz}
	\comment{Ligature {\it tz}, traditional in {\it Fraktur} fonts.}
\endsetslot

\setslot{schwa}
	\comment{The sign for schwa (indeterminate vowel sound), an
	inverted {\it e}.  The most commonly used pronounciation
	symbol.}
\endsetslot

\setslot{l1}
	\comment{Variant letter {\it l}.  Malvern-specific.}
\endsetslot

\setslot{lacute1}
	\comment{Variant letter {\it \'l}.  Malvern-specific.}
\endsetslot

\setslot{lcaron1}
	\comment{Variant letter {\it l\kern-0.1em'} (\thinspace= {\it \v l}).  Malvern-specific.}
\endsetslot

\setslot{lslash1}
	\comment{Variant letter {\it \l}.  Malvern-specific.}
\endsetslot

\setslot{ll1}
	\comment{Variant ligature {\it ll}.  Malvern-specific.}
\endsetslot

\nextslot{`\|}
\setslot{bar}
    \comment{Vertical bar character, `$|$'.}
\endsetslot

\nextslot{`\~}
\setslot{asciitilde}
    \comment{A swung dash, `$\sim$'.}
\endsetslot

\nextslot{128}
\setslot{arrowdblleft}
    \comment{A doubled arrow pointing left, `$\Leftarrow$'.}
\endsetslot

\setslot{arrowdblright}
    \comment{A doubled arrow pointing right, `$\Rightarrow$'.}
\endsetslot

\setslot{arrowdblup}
    \comment{A doubled arrow pointing up, `$\Uparrow$'.}
\endsetslot

\setslot{arrowdbldown}
    \comment{A doubled arrow pointing down, `$\Downarrow$'.}
\endsetslot

\setslot{arrowdblboth}
    \comment{A doubled arrow pointing left and right, `$\Leftrightarrow$'.}
\endsetslot

\setslot{arrowdblupdown}
    \comment{A doubled arrow pointing up and down, `$\Updownarrow$'.}
\endsetslot

\setslot{arrowdblhorizex}
    \comment{A pair of rules that can be used to extend horizontal
	arrows (to make `$\Longrightarrow$' etc.).}
\endsetslot

\setslot{arrowdblvertex}
    \comment{A rule that can be used to extend vertical arrows.}
\endsetslot

\nextslot{"A0}
\setslot{visiblespace}
    \comment{A symbol representing an ASCII blank.  In CM Typewriter it
	is `{\tt\char32 }'.}
\endsetslot

\setslot{onequarter}
    \comment{The fraction `\frac1/4'.}
\endsetslot

\setslot{onehalf}
    \comment{The fraction `\frac1/2'.}
\endsetslot

\setslot{threequarters}
    \comment{The fraction `\frac3/4'.}
\endsetslot

\setslot{logicalnot}
    \comment{Sign for logical negation `$\lnot$'.}
\endsetslot

\setslot{mu}
    \comment{A lower case Greek letter mu `$\mu$'.}
\endsetslot

\nextslot{"B0}
\setslot{zeroinferior}
    \comment{Inferior numeral zero, `\lower0.5ex \hbox{\the\scriptfont0 0}'.}
\endsetslot

\setslot{oneinferior}
    \comment{Inferior numeral one, `\lower0.5ex \hbox{\the\scriptfont0 1}'.}
\endsetslot

\setslot{twoinferior}
    \comment{Inferior numeral two, `\lower0.5ex \hbox{\the\scriptfont0 2}'.}
\endsetslot

\setslot{threeinferior}
    \comment{Inferior numeral three, `\lower0.5ex \hbox{\the\scriptfont0 3}'.}
\endsetslot

\setslot{fourinferior}
    \comment{Inferior numeral four, `\lower0.5ex \hbox{\the\scriptfont0 4}'.}
\endsetslot

\setslot{fiveinferior}
    \comment{Inferior numeral five, `\lower0.5ex \hbox{\the\scriptfont0 5}'.}
\endsetslot

\setslot{sixinferior}
    \comment{Inferior numeral six, `\lower0.5ex \hbox{\the\scriptfont0 6}'.}
\endsetslot

\setslot{seveninferior}
    \comment{Inferior numeral seven, `\lower0.5ex \hbox{\the\scriptfont0 7}'.}
\endsetslot

\setslot{eightinferior}
    \comment{Inferior numeral eight, `\lower0.5ex \hbox{\the\scriptfont0 8}'.}
\endsetslot

\setslot{nineinferior}
    \comment{Inferior numeral nine, `\lower0.5ex \hbox{\the\scriptfont0 9}'.}
\endsetslot

\endencoding

\Aheading{End of encoding.}

\bye

%Local Variables:
%tex-has-children: t
%fill-prefix: "	"
%End:
