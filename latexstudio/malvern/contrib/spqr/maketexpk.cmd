/*    

Translated from Piet Tutelaar's Unix shell script 
into Rexx by Sebastian Rahtz, 15.8.92 (spqr@minster.york.ac.uk)

   This script file makes a new TeX PK font, because one wasn't
   found.  Parameters are:

   name dpi bdpi magnification [[mode] subdir]

   `name' is the name of the font, such as `cmr10'.  `dpi' is
   the resolution the font is needed at.  `bdpi' is the base
   resolution, useful for figuring out the mode to make the font
   in.  `magnification' is a string to pass to MF as the
   magnification.  `mode', if supplied, is the mode to use.

   Note that this file must execute MetaFont (and then gftopk) or
   ps2pk, and place the result in the correct location for the
   PostScript driver to find it subsequently.  If this doesn't work,
   it will be evident because the program will be invoked over and over again.

   If no METAFONT source is available for `name' and the fontname
   starts with a `r' or is available in $TEXCONFIG/adobe then the 
   program tries to find a type1 font. If such a font is located 
   ps2pk will be used to render the font.

   Of course, it needs to be set up for your site with regard to paths etc
*/

PARSE Arg all_args
argc=words(all_args)
if ( argc < 4  | argc > 6 ) then 
do
   say "Usage: maketexpk fontname dpi bdpi magnification [[mode] subdir]"
   exit 1
end

call RxFuncAdd 'SysLoadFuncs', 'RexxUtil', 'SysLoadFuncs'
call SysLoadFuncs
os2env='OS2ENVIRONMENT'

/* destination of pk files */
DESTDIR='d:\texfonts\pixel.lj'

/* Where we look for METAFONT sources */
MFINPUT=value('MFINPUT',,os2env)
if MFINPUT='' then
do
	MFINPUT='d:\emtex\moremf;d:\emtex\mfinput;d:\emtex\spqr\fonts'
	MFINPUT=value('MFINPUT',MFINPUT,os2env)
	MFINPUT=value('MFINPUT',,os2env)
end


/* Where we look for Type1 fonts and AFM files */
T1INPUTS=value('T1INPUTS',,os2env)
if T1INPUTS='' then
do
	T1INPUTS='d:\emtex\ps'
	T1INPUTS=value('T1INPUTS',T1INPUTS,os2env)
	T1INPUTS=value('T1INPUTS',,os2env)
end

/* Where we look for dvips stuff */
TEXCONFIG='d:\emtex\ps'
listoftype1files=TEXCONFIG'\fontname'

/* What encoding scheme do we use for Type1 text fonts? */

/* DEF_TEXT_ENCODING="-eEC.enc"  (Extended Computer Modern!) */
DEF_TEXT_ENCODING=''

/* 
 TEMPDIR needs to be unique for each process because of the possibility
 of simultaneous processes running this script.
*/
TEMPDIR=SysTempFileName("d:/tmp/mtpk.???")

NAME=word(all_args,1)
DPI=word(all_args,2)
BDPI=word(all_args,3)
MAG=word(all_args,4)
if argc > 4 then 
	MODE=word(all_args,5)
else
   if BDPI = 300   then
      MODE='hplaser'
   else if BDPI = 1270   then
      MODE='linohi'
   else if BDPI = 2540   then
      MODE='linosuper'
   else
    do
      say 'I do not know the mode for ' BDPI
      say 'update this MakeTeXPK script'
      exit 1
    end

if argc > 5 then 
	SUBDIR=word(all_args,6) 
else
	SUBDIR=BDPI'dpi'


PKNAME=NAME'.pk'
DESTDIR=DESTDIR'\'DPI'DPI'
/*
say 'making for ' MODE ' in ' DESTDIR SUBDIR 
say 'temporary output file is 'TEMPDIR'\'PKNAME
*/
call sysfiletree DESTDIR ,'test', 'OD'
if test.0=0 then
      call SysMkDir DESTDIR
/*
else
      say 'directory' DESTDIR 'exists'
*/
destpk=DESTDIR'\'PKNAME
fname = stream(destpk , 'C', 'QUERY EXISTS')
if \ (fname ='') then
  do
     say DESTDIR'\'PKNAME 'already exists!'
    exit 0
  end

/* OK, try to make the font by some means */

/* first try Adobe Type1 fonts */

/* Strip off the possible starting `r' */
if substr(NAME,1,1)='r' then
	VNAME=substr(NAME,2)
else
	VNAME=NAME
call sysfilesearch VNAME' ',listoftype1files, 'FILE.','C'
if rc=3 then
do
   say 'You should install file ' listoftype1files
   exit 0
end
FULLNAME='!'
if FILE.0>0 then 
   do i=1 to file.0
	if word(file.i,1)=VNAME then 
		do
		FULLNAME=word(file.i,2)
		leave
		end
   end
if FULLNAME='!' then  /* failed to locate a name in the list 

  Do we have a METAFONT source for this typeface? 
*/
  do
	if substr(NAME,1,3)='fmv' then 
			mfname=malvern(NAME)  /* Malvern font */ 
	else
	   mfname=syssearchpath('MFINPUT',NAME'.mf')
   say 'Result of MF search: 'mfname
   if mfname='' then
    do
	      say 'Sorry, no PostScript font or Metafont font found' 
	      exit 1
    end
   job='@mfp \mode:='MODE'; mag:='MAG'; scrollmode; input 'name
/* 
really no need to use a temporary directory, I think, in this environment

   curdir=directory()
   call SysMkDir TEMPDIR
   newdir= directory(TEMPDIR)
*/
   job
   if substr(NAME,1,3)='fmv' then  /* Malvern font */ 
	'del 'NAME||'.mf'
   GFNAME=NAME'.'DPI
   if  (stream( GFNAME, 'C', 'QUERY EXISTS') = '') then
    do
      echo 'Metafont failed for some reason on 'GFNAME
      exit 1
    end
'@gftopk' GFNAME PKNAME
   '@copy' PKNAME destpk 
   x=sysfiledelete(GFNAME)
   x=sysfiledelete(PKNAME) 
   x=sysfiledelete(NAME'.log')
   x=sysfiledelete(NAME'.tfm') 
   exit 0
 end
else
/*  we have a Type1 font (at any rate, its in the list!) */
   afmname=syssearchpath('T1INPUTS',VNAME'.afm')
   if afmname='' then
      do
        say 'Failed to find afm file for' VNAME ' in 'T1INPUTS
        exit 1
      end
/*  If we don't use default AFM encoding then we have to check
   if we can apply this encoding (text fonts only) 
*/
ENCODING=DEF_TEXT_ENCODING

   if \ (DEF_TEXT_ENCODING = '')   then
    do
    /* What is encoding scheme that the AFM file uses? */
    call sysfilesearch 'EncodingScheme',afmname, 'FILE.','C'      
	if file.0=0  then
        do
         say afmname': Invalid AFM file!'
         exit 0
        end
      EXT_ENC=word(file.1,2)
      select
      when substr(ext_end,1,6)='AdobeS' then ENCODING=DEF_TEXT_ENCODING
      when substr(ext_end,1,3)='ISO' then ENCODING=DEF_TEXT_ENCODING
      otherwise
	     ENCODING=''
      end
   end
   pfbname=syssearchpath('T1INPUTS',VNAME'.pfa')
   if pfbname='' then
    do
      pfbname=syssearchpath('T1INPUTS',VNAME'.pfb')
      if pfbname='' then
        do
         say 'Source for font' vname 'not found on path 'T1INPUTS
         exit 1
        end
   end
   job='@ps2pk -X'DPI ENCODING filespec('name',pfbname) destpk 
   job
exit 0

malvern: procedure
arg fontname
/* Malvern generation
#
#  The font name is of the form
#
#    	fmvWVV##		Karl Berry's standard font names
#
#  where W   denotes a weight (one of t, i, l, k, r, d, b, x, c)
#        VV  denotes a "variation" (one of <empty>, rn, re, i, in, ie)
#	 ##  is the design size, in points
#
#  or
#	maXXx##			my nonstandard font names
#	
#  where XX is 2 digits giving a style in the tradition of Univers
#        x  is a suffix describing the character set of the font
#	 ## is the design size, in points (with p as decimal point)
#
#  Examples:
#
#    fmvr10	ma55a10		Malvern 55 10-pt
#    fmvd10	ma65a10		Malvern 65 demibold 10-pt
#    fmvbix18	ma74a18		Malvern 74 bold extended italic 18-pt
#    fmvric7	ma58a7		Malvern 58 condensed italic 7-pt
#
#  The fmv- fonts all use the Cork encoding.
#
*/
weights.=''
weights.1= 'weight = 1/4;'	/* ultra-light */
weights.2= 'weight = 1/2;'	/* extra-light */
weights.3= 'weight = 3/4;'	/* light */
weights.4= 'weight = 7/8;'	/* book */
weights.5= 'weight = 1;'	/* medium */
weights.6= 'weight = 1.5;'	/* demi  */
weights.7= 'weight = 2;'	/* bold  */
weights.8= 'weight = 3;'	/* extra */
weights.9= 'weight = 4;'	/* ultra */

weights.t= 'weight = 1/4;'	/* thin [ultra-light] */
weights.i= 'weight = 1/2;'	/* extra-lIght */
weights.l= 'weight = 3/4;'	/* Light */
weights.k= 'weight = 7/8;'	/* booK */
weights.r= 'weight = 1;'	/* Regular */
weights.d= 'weight = 1.5;' 	/* Demi */
weights.b= 'weight = 2;'	/* Bold */
weights.x= 'weight = 3;'	/* eXtra */
weights.u= 'weight = 4;'	/* Ultra */

styles.=''

styles.9='hratio = 0.3;'
styles.0='hratio = 0.3; slant := 1/8; italicness = 1;'
styles.7='hratio = 0.6;'
styles.8='hratio = 0.6; slant := 1/8; italicness = 1;'
styles.5='hratio = 1;'
styles.6='hratio = 1; slant := 1/8; italicness = 1;'
styles.3='hratio = 1.15;'
styles.4='hratio = 1.15; slant := 1/8; italicness = 1;'
styles.1='hratio = 1.30;'
styles.2='hratio = 1.30; slant := 1/8; italicness = 1;'

styles.i='italicness = 1; slant := 1/8;'	/* italic */
styles.u='italicness = 1;'	 	/* vertical italic */
styles.o='slant := 1/8;'			/* oblique */

/* Berry's system has expansion after variant rather than before: */
styles.o='hratio = 0.3;'	/* narrow (extra condensed) */
styles.c='hratio = 0.6;'	/* compressed */
styles.x='hratio = 1.15;'	/* expanded */
styles.w='hratio = 1.30;' 	/* wide (extra expanded) */

/* not used here
-J 's=encoding=0'	# Standard TeX text (like cmr)
-J 'x=encoding=-200'	# Cork (TUGboat 11#4)
-J 'mi=encoding=-1'	# maths (like cmmi)
-J 'sy=encoding=-2'	# symbol (like cmsy)
-J 'ps=encoding=-1000'	# the AdobeStandardEncoding encoding
-J 'so=encoding=-1010'	# the ISOLatin1Encoding encoding

#  Malvern encodings:
-J 'a=encoding=1'	# Malvern A (simple letters)
-J 'b=encoding=2'	# Malvern B (strange symbols)
-J 'c=encoding=3'	# Malvern C (composites)
*/
rf=reverse(fontname)
parse var rf n1 +1 n2 +1 .
if datatype(n2,'N')=1 then
  size=n2||n1
else
  size=n1
parse var fontname basename (size) .
parse var basename base +3 weight +1 style +1 expansion 
if style\=' ' then
  y=value(style)
else
  y=''
if expansion\=' ' then
  z=value(expansion)
else 
  z=''
x=value(weight)
fontfile=value(fontname)||'.mf'
call lineout fontfile,'font_size 'size'pt#; 'weights.x styles.y styles.z 'input maltex;bye.'
call lineout fontfile 
say 'generated Malvern file ' fontfile
return fontfile
