% ma.mf 1.2.0 94/10/11 -- top-level driver file for the Malvern family
% Copyright  1991, 1992, 1993, 1994 P. Damian Cugley

%%% @METAFONT-file {
%%%   filename      = "ma.mf",
%%%   version       = "1.2.0",
%%%   package       = "Malvern 1.2",
%%%   author        = "P. Damian Cugley",
%%%   email         = "damian.cugley@comlab.ox.ac.uk",
%%%   address       = "Oxford University Computing Laboratory,
%%%                    Parks Road, Oxford  OX1 3QD, UK",
%%%   codetable     = "USASCII",
%%%   keywords      = "Malvern, METAFONT, font, typefont, TeX",
%%%   supported     = "Maybe",
%%%   abstract      = "Generic driver file for the Malvern
%%%                    font family.",
%%%   dependencies  = "other program files",
%%% }

%  See the Malvern Handbook (maman.tex) for more info about Malvern.
%  This software is available freely without warranty.
%  See the file COPYING for details.

%{{{ ma.mf
string maversion; maversion = "1.2";

boolean new_mf; new_mf = unknown no_new_mf;

%{{{  Just testing?

%  If |mode| is unknown (no |\mode| in commandline) 
%  then |testing| is given value |true|.
%  This means that most character programs will be skipped.

%  To make a proof of all charcters, use |\mode=proof;...|.

boolean testing;

if unknown mode:
    message "Malvern version " & maversion;
    message "Copyright (c) 1991 Damian Cugley.";
    if designsize = 0pt#: font_size 10pt#; fi
    testing := true;
else:
    testing := false;
fi

%}}}
mode_setup;
input maparams
input maencode
input makit;
%{{{  read program files

code_offset = 0;    	    	% added to all character codes

boolean no_co;	% control which chars made by macaps.
no_co := false;

boolean lc_marks; 	lc_marks = true;
boolean text_figs;	text_figs = true;
boolean small_figs; 	small_figs = false; % sup or inf figs
fig_b = 0v;

height# := 12v#; 
pn_wd# := pn.wd#;

if testing: endinput fi

input mapunct		% punctuation
input masyms		% symbols

if known code.lc.a or known code.lc.longs:
    	         	input malc     
fi	  % lowercase letters
if known code.mk.acute: input mamarks  fi	  % marks for l.c.
%%%%if known code.gr.lc.mu: input magrlc   fi	  % l.c. greek
input magrlc
if known code.gr.cap.gamma: input magrcaps fi	  % u.c. greek
if known code.cy.cap.b:	input macy     fi	  % cyrillic

%{{{   Figures 0-9

if known code.zero.old_style:
    text_figs := true;
    fig_offset := code.zero.old_style;
    input mafigs; 
fi

if known code.zero: 
    text_figs := false;
    fig_b := 0; fig_dp# := 0;
    fig_offset := code.zero;
    input mafigs;
fi

if known code.zero.superior:
    text_figs := false;
    fig_ht# := body_ht#; 
    fig_wd# := sfig_wd#;
    fig_b := vround (body_ht - sfig_ht); fig_dp# := 0;
    fig_offset := code.zero.superior;
    input mafigs;
fi

if known code.zero.inferior:
    text_figs := false;
    fig_ht# := -body_dp# + sfig_ht#; 
    fig_wd# := sfig_wd#;
    fig_b := vround (-body_dp); fig_dp# := body_dp#;
    fig_offset := code.zero.inferior;
    input mafigs;
fi

%}}}
%{{{   Capital letters

%%  The conventions for capital letters programs are supposed
%%  to've been changed -- again -- to use capcode instead of
%%  all these offsets and things.

def input_macaps(suffix CC, SS) =
    if known code.offset.CC and known code.offset.SS:
    	if code.offset.SS <> code.offset.CC:
	    co_only := true;  no_co := false;
	    message (str CC & " -- composites.")
	    code_offset := code.offset.CC; scantokens "input macaps";
	    co_only := false;  no_co := true;
	    message(str SS & " -- simples.")
	    code_offset := code.offset.SS; scantokens "input macaps";
        else:  
    	    co_only := no_co := false;
    	    code_offset := code.offset.SS; 
    	    message(str SS & "&" & str CC & " -- simples & composites.");
    	    scantokens "input macaps";
        fi
    else:  % one or the other unknown
    	if known code.offset.CC: 
    	    co_only := true; no_co := false;
    	    code_offset := code.offset.CC;  show code.offset.CC;
    	    message (str CC & " -- composites only.");
	    scantokens "input macaps";
    	else: 
	    co_only := false; no_co := true;
	    code_offset := code.offset.SS;
	    message (str SS & " -- simples only.");
	    scantokens "input macaps";
    	fi;
    fi
enddef;

if known code.offset.LS or known code.offset.LC:
    capmode := 1;
    pickup pencircle xscaled cap.pn.wd yscaled cap.pn.ht; the_pen := savepen;
    height# := cap_ht#; 	pn_wd# := cap.pn.wd#;
    input_macaps(LC,LS);
fi

if known code.mk.Acute:
    code_offset := code.mk.Acute - code.mk.acute;
    lc_marks := false; 	input mamarks;
fi

if known code.offset.MC or known code.offset.MS:
    capmode := 2;
    pickup pencircle xscaled mcap.pn.wd yscaled mcap.pn.ht; the_pen := savepen;
    height# := mcap_ht#;	pn_wd# := mcap.pn.wd#;
    input_macaps(MC,MS);
fi

if known code.offset.SC or known code.offset.SS:
    capmode := 3;
    pickup pencircle xscaled scap.pn.wd yscaled scap.pn.ht; the_pen := savepen;
    height# := scap_ht#; 	pn_wd# := scap.pn.wd#;   
    input_macaps(SC,SS);
fi

%}}}
%}}}
if known code.lc.a and known code.offset.LS:
%{{{  Ligtable

vardef chardefined primary c =
    if known byte c: charexists c else: false fi
enddef;

def KK = kern -2ku# enddef;
def K = kern -3/2ku# enddef;
def k = kern -ku# enddef;
def kk = kern -1/2ku# enddef;
def kkk = kern -1/4ku# enddef;

def o_kerns primary x =
    if italic: "a" kern x, fi 
    for i = "c", "d", "e", "g", "o": i kern x, endfor
    "q" kern x
enddef;

def O_kerns primary x =
    for i = "C", "G", "O": i kern x, endfor
    "Q" kern x
enddef;

def u_kerns primary x = 
    "u" kern x, "y" kern x
enddef;

def v_kerns primary x = 
    "v" kern x, "w" kern x
enddef;

def maybe_a_kern primary x =
    if not italic: "a" kern x, fi
enddef;
    
def kerns_for_f =
    if new_mf: 1:: fi
    maybe_a_kern(-1/2ku#)
    o_kerns(-1/2ku#), 
    "s" kk,
    if new_mf: boundarychar kern 1.5u#, fi
    "," k, "." k,
    "'" kern ku#, "?" kern ku#, ")" kern ku#, "]" kern ku#, 
    if known code.anglebr.right: code.anglebr.right kern ku#, fi
    if known code.lc.dotless_j and new_mf: % fjord
	"j" |=: code.lc.dotless_j,
    fi
    if known code.lc.dotless_i: 
    	code.lc.dotless_j kern fi_kern#,
        code.lc.dotless_i kern fi_kern#,
    fi
    "*" kern ku#
enddef;

def do_kerns_for_f = if new_mf: skipto 1 else: kerns_for_f fi enddef;

ligtable "b": "e": "o": "p":
    "f" kkk, "t" kkk,
    "v" kern -1/3ku#, "w" kern -1/3ku#,
    ")" k;  

ligtable "c":
    o_kerns(-1/2ku#)
    if known c_h_kern#: if c_h_kern# <> 0pt#: , "h" kern c_h_kern# fi fi;

ligtable "i": "j":
    "j" kern -0.125ku#;

ligtable "l":
    "l" kern 1/8ku#;
    
ligtable "r":
    maybe_a_kern(-3/4ku#) o_kerns(-3/4ku#),
    "j" kkk, "s" kk;

if chardefined code.lc.f.f:
ligtable "f":
    if chardefined code.lc.f.f: "f" =: code.lc.f.f, fi
    if chardefined code.lc.f.i: "i" =: code.lc.f.i, fi
    if chardefined code.lc.f.l: "l" =: code.lc.f.l, fi
    do_kerns_for_f;
fi

if chardefined code.lc.f.f.i:
    ligtable code.lc.f.f: 
    	"i" =: code.lc.f.f.i, "l" =: code.lc.f.f.l,
        kerns_for_f;
fi

ligtable "A":
    o_kerns(-1/4ku#), 
    "j" kk, "t" kkk, "v" k, "w" k, 
    O_kerns(-1/2ku#),
    "S" kk, "U" kk, 
    "T" k, "V" K, "W" k, "Y" K;

ligtable "D":  "O":  "Q":
    "A" kk, ")" k;

ligtable "F":
    maybe_a_kern(-ku#)
    o_kerns(-ku#);

ligtable "L":
    maybe_a_kern(-1/2ku#)
    o_kerns(-ku#), u_kerns(-1/2ku#), v_kerns(-ku#);

ligtable "P":
    "A" k,
    "." kern -1.5ku#, "," kern -2ku#;

ligtable "T":
    maybe_a_kern(-2ku#) 
    o_kerns(-2ku#), "r" K, "s" KK,
    u_kerns(-2ku#), "w" k,
    "A" k, 
    if hratio >= 0.9: "C" k, "G" k, "O" k, "Q" k, fi
    "." kern -1.5ku#, "," kern -1.5ku#,
    "J" k,
    "T" kern 1/2ku#;

ligtable "V": 
    "A" K;

ligtable "W":
    "A" k;

ligtable "Y":
    "A" K;

ligtable ".":
    "." kern 2ku#, "?" kern 4ku#, "!" kern 4ku#;

ligtable "3":
    "0" k, "r" kk;

ligtable "4":
    "t" k;

ligtable "(": "[": if chardefined code.anglebr.left: code.anglebr.left: fi
    O_kerns(-ku#), o_kerns(-ku#), "T" kern +1/2ku#;

if chardefined code.en_dash:
    ligtable "-": "-" =: code.en_dash;
    ligtable code.en_dash: "-" =: code.em_dash;
fi

if chardefined code.dbl.inv.comma:
    ligtable "'": "'" =: code.dbl.apostrophe;
    ligtable "`": "`" =: code.dbl.inv.comma;
fi

if chardefined code.dbl.comma:
    ligtable ",": "," =: code.dbl.comma;
fi

if chardefined code.guillemet.single.left:
    ligtable code.guillemet.single.left: 
    	code.guillemet.single.left =: code.guillemet.left;
    ligtable code.guillemet.single.right: 
    	code.guillemet.single.right =: code.guillemet.right;
fi

if chardefined code.inv.question:
    ligtable "?": "`" =: code.inv.question;
    ligtable "!": "`" =: code.inv.exclam;
fi
%}}}
fi
%{{{  Fontdimens

font_slant  	    slant;
font_x_height	    x_ht#;
font_quad   	    16u#;      % = design_size * hratio
font_normal_space   5u#;       % 0.313em
font_normal_stretch 3u#;       % max space is 1/2 em
font_normal_shrink  2u#;       % min is 0.2 em (ouch)
font_identifier     ("MA"
    & decimal weight & "/" & decimal hratio & "/"
    & decimal slant & if italic: "I" else: "R" fi
    & if encoding = 0: "S" else: char (64 + encoding) fi);

subs = 0.7; 	    	% expect subscripts to be 70% as big
rth# = 0.04designsize;	% expect rule of 0.4pt when 10pt
fontdimen8: axis_ht# + 3.5rth# + 2.5v# + subs*desc_dp#;  % num1  (display)
fontdimen9: axis_ht# + 1.5rth# + 1.5v#;	    % num2  (\over, non-display)
fontdimen10: axis_ht# + 1.5rth# + 2.5v#;    % num3  (\atop, non-display)
fontdimen11: -(axis_ht# - 3.5rth# - subs*fig_ht# - 6v#);  % denom1  (display)
fontdimen12: -(axis_ht# - 1.5rth# - subs*fig_ht# - 1.5v#); % denom2 (text)
fontdimen13: body_ht# - subs*0.75x_ht#; % sup1 (non-cramped, display)
fontdimen14: body_ht# - subs*x_ht#;    	% sup2 (non-cramped, non-display)
fontdimen15: body_ht# - subs*0.75[asc_ht#,x_ht#]; % sup3 (cramped)
fontdimen16: 3v#; %  sub1
fontdimen17: min(-(body_ht# - subs*0.75[asc_ht#,x_ht#] - 2v# - subs*asc_ht#), 
                 body_dp#);  % sub2
fontdimen18: subs*asc_ht# - 1/2v#;  % sup_drop
fontdimen19: 0.9 v#; 	    	    % sub_drop
fontdimen20: 36v#; % delim1 (display)
fontdimen21: 18v#;	% delim2 (non-display)
fontdimen22: axis_ht#;

%  Karl Berry's suggestions:
fontdimen23: body_ht# + min(1,weight/hratio) * 0.2 * body_ht#;
fontdimen24: body_dp# + min(1,weight/hratio) * 0.2 * body_dp#;
fontdimen25: designsize;

%}}}
%}}} ma.mf

%Local variables:
%fold-folded-p: t
%End:
