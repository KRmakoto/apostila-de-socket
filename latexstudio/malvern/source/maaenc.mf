% maaenc.mf 1.2.0 1994/10/11 -- Malvern Encoding A
% Copyright  1993-4 P. Damian Cugley.

%%% @METAFONT-file {
%%%   filename       = "maaenc.mf",
%%%   version        = "1.2.0",
%%%   date           = "1994/10/11",
%%%   package        = "Malvern 1.2",
%%%   author         = "P. Damian Cugley",
%%%   email          = "damian.cugley@comlab.ox.ac.uk",
%%%   address        = "Oxford University Computing Laboratory,
%%%                     Parks Road, Oxford  OX1 3QD, UK",
%%%   codetable      = "USASCII",
%%%   keywords       = "Malvern, METAFONT, font, typefont, TeX",
%%%   supported      = "Maybe",
%%%   abstract       = "Character programs for the Malvern
%%%                     font family, to be compiled using
%%%                     METAFONT into TFM files (for TeX) and
%%%                     GF files (for your printer driver).",
%%%   dependencies   = "other program files",
%%% }

%  See the Malvern Handbook (maman.tex) for more info about Malvern.
%  This software is available freely but without warranty.
%  See the file COPYING for details.

%{{{ maaenc.mf

%  These offsets are now obselete:
code.offset.LS = code.offset.MS = code.offset.SS = 0;

%{{{   Summary

%         0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
%       +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
%    0  |     l.cap specials    | *   * |   marks that go above l.c.    |
%       +                       +---+---+---+---+---+---+---+---+---+---+
%    1  |      l.c. specials    | .  .. |    l.c.  ligs, eszet, \i, \j  |
%       +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
%    2  | *   !  TM   #   $   %   &   '   (   ) | *   +   ,   -   .   / |
%       +---+---+---+---+---+---+---+---+---+---+                       +
%    3  |      text/old style figures           | :   ;   <   =   >   ? |
%       +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
%    4  | @ |                                                           |
%       +---+              large capitals           +---+   +---+       +
%    5  |                                         Z | [ | O | ] |AE  OE |
%       +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
%    6  | ` |                                                           |
%       +---+              lower case               +---+   +---+       +
%    7  |                                           | { | o | } |ae  oe |
%       +===+===+===+===+===+===+===+===+===+===+===+===+===+===+===+===+
%    8  |     m.cap specials    | *   *	|  marks that go above m.caps   |
%       +                       +---+---+---+---+---+---+---+---+---+---+
%    9  |     s.cap specials    | -  -- |     really weird stuff        |
%       +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
%    A  |    !`   c   *   *  %%   x   ''  Y   f | *   *  ,,  --   .  /  |
%       +---+---+---+---+---+---+---+---+---+---+                       +
%    B  |       ranging figures                 |\S  \P  <<  --- >>  ?` |
%       +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
%    C  | * |                                                           |
%       +---+              medium capitals          +---+   +---+       +
%    D  |                                           | < | O | > |AE  OE |
%       +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
%    E  |`` |                                                           |
%       +---+              small capitals           +---+   +---+       +
%    F  |                                           |(C)| O |(R)|AE  OE |
%       +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
%}}}
%{{{   0-5, 16-21   -- special letters that appear in l.c. and caps 

%  These have m.caps and s.caps equivs at +128 and +144:

tmp := 0 - 1;
code.cap.thorn      = incr tmp;  	code.lc.thorn	    = 16 + tmp;
code.cap.a.ogonek   = incr tmp;		code.lc.a.ogonek    = 16 + tmp;
code.cap.eng	    = incr tmp;  	code.lc.eng	    = 16 + tmp;
code.cap.l.slash    = incr tmp;  	code.lc.l.slash	    = 16 + tmp;
code.cap.eth        = incr tmp;  	code.lc.eth    	    = 16 + tmp;
code.cap.e.ogonek   = incr tmp;	    	code.lc.e.ogonek    = 16 + tmp;
%}}}
%{{{   6, 7, 8-15, 22, 23  -- MARKS

%  Marks are added to some letters to make compoite characters.
%  Some that go above letters are drawn differently for m.caps and l.c.
%  The l.c. version is also for s.caps, and the m.caps version for large caps.
%  Dots, twodots, macron and maacron appear once only.

tmp := 8 - 1;
% These have m.caps equivs at +128:
code.mk.acute	= incr tmp;	code.mk.Acute	= tmp + 128;
code.mk.grave  	= incr tmp; 	code.mk.Grave  	= tmp + 128;
code.mk.circumflex = incr tmp;	code.mk.Circumflex = tmp + 128;
code.mk.tilde  	= incr tmp;	code.mk.Tilde  	= tmp + 128;
code.mk.ring    = incr tmp; 	code.mk.Ring    = tmp + 128;

code.mk.hook   	= incr tmp;	code.mk.Hook   	= tmp + 128;
% in Czech, called a h\'a\v{c}ek.  AKA caron

code.mk.breve 	= incr tmp; 	code.mk.Breve 	= tmp + 128;
code.mk.hungarian = incr tmp;	code.mk.Hungarian = tmp + 128;
code.mk.cedilla	= 6; 		code.mk.Cedilla	= 128 + code.mk.cedilla;
code.mk.ogonek	= 7;		code.mk.Ogonek	= 128 + code.mk.ogonek;

%  These appear once only:
code.mk.dot 	= 22;
code.mk.twodots	= 23;
code.mk.macron	= 22 + 128;
code.mk.maacron	= 23 + 128;	% macron for 2 chars

%}}}
%{{{   24-31 -- l.c.-only ligs and special letters

tmp := 24 - 1;
code.lc.eszet  	= incr tmp;
code.lc.dotless_i = incr tmp;
code.lc.dotless_j = incr tmp;
code.lc.f.f    	= incr tmp;
code.lc.f.i	= incr tmp;
code.lc.f.l 	= incr tmp;
code.lc.f.f.i	= incr tmp;
code.lc.f.f.l	= incr tmp;

%}}}
%{{{   "!"-"?" -- symbols and punct

%  Most are as ASCII except for 
%   ""	    	trademark -- this should encourage people to 
%   	    	remember to use '' for double quote marks
%   '  	    	apostrophe (only)
%   -	    	hyphen (only)
%   < >	    	single guillemets
%   0123456789	old-style numerals

input ascii
%  defines a common ASCII subset

code.bullet	= 32;

code.trademark 	= 34; % replaces doublequote (this is intentional)
code.hyphen 	= ASCII"-";

code.zero.old_style = ASCII"0";	    % defines all old-style figures

code.guillemet.single.left	= ASCII"<";
code.guillemet.single.right	= ASCII">";

%}}}
%{{{   "A"-"Z", "a"-"z" etc.

code.cap.o.slash	= hex"5C";  	code.lc.o.slash	= hex"7C";
code.cap.ae		= hex"5E";  	code.lc.ae    	= hex"7E";
code.cap.oe		= hex"5F";  	code.lc.oe    	= hex"7F";

code.brace.left	= 123; 	
code.brace.right= 125;

%}}}
%{{{   128-159 -- cap versions of 0-31 and extra syms

code.mc.thorn	    	= hex"80";
code.mc.a.ogonek    	= hex"81";
code.mc.eng 	    	= hex"82";
code.mc.l.slash	    	= hex"83";
code.mc.eth 	    	= hex"84";
code.mc.e.ogonek    	= hex"85";
code.sc.thorn	    	= hex"90";
code.sc.a.ogonek    	= hex"91";
code.sc.eng 	    	= hex"92";
code.sc.l.slash	    	= hex"93";
code.sc.eth 	    	= hex"94";
code.sc.e.ogonek    	= hex"95";
code.ord_feminine 	= hex"98";	% Na.
code.ord_masculine  	= hex"99";	% No.
code.care_of		= hex"9A";  	% c/o
code.sp.lc.c  	= hex"9B";  	% as in McDonald
code.lc.d.hook	= hex"9C";  	% d' -- Czech d with hook
code.lc.t.hook	= hex"9D";  	% t' -- Czech t with hook
code.lc.h.circumflex = hex"9E";	% \^h -- Esperanto hho
code.lc.l.hook	= hex"9F";  	% l' -- Slovakian?

%}}}
%{{{   meta"!"..meta"?" -- symbols and more punct

code.ballotbox 	= meta " ";
code.inv.exclam	= meta "!";    % !`  (should that be exclam.inv?)
code.cent  	= hex"A2";
code.sterling 	= meta "#";    % \pounds
code.currency	= meta "$";    % ISO 646 currency sign
code.per_thousand = meta "%";
code.multiply  	= meta "&";    % as in 2x4 ...
code.yen    	= meta "(";
code.florin	= meta ")";
code.dbl.apostrophe = meta "'";	% ''
code.dagger	= meta "*"; 	% \dag
code.dbl.dagger	= meta "+"; 	% \ddag
code.dbl.comma	= meta ",";	% ,,
code.en_dash	= meta "-";	% --
code.cdot   	= meta ".";	% $\cdot$
code.minus_sign	= meta "/";   	% $-$ 
code.zero 	= meta "0";	% ranging figures start here
code.section	= meta ":"; 	% \S
code.paragraph	= meta ";"; 	% \P
code.guillemet.left = meta "<";	% left-pointing french quote
code.em_dash	= meta "=";	% ---
code.guillemet.right = meta ">";	% right-pointing french quote
code.inv.question = meta "?";	% ?`

%}}}
%{{{   meta"A"-meta"Z", meta"a"-meta"z" -- med caps, small caps

code.degree		= meta "@";    % as in angles etc.
code.dbl.inv.comma 	= meta "`";    % ``
code.copyright		= meta "{";
code.registered		= meta "}";
code.anglebr.left 	= meta "[";
code.anglebr.right	= meta "]";
code.mc.o.slash	    	= hex"DC";
code.mc.ae  	    	= hex"DE";
code.mc.oe  	    	= hex"DF";
code.sc.o.slash	    	= hex"FC";
code.sc.ae  	    	= hex"FE";
code.sc.oe  	    	= hex"FF";

%}}}

for i = ASCII"a" step 1 until ASCII"z":
    scantokens ("code.lc." & char i) = i;
    scantokens ("code.cap." & char i) = i - 32;
    scantokens ("code.sc." & char i) = i + 128 - code.offset.SS;
    scantokens ("code.mc." & char i) = i - 32 + 128 - code.offset.MS;
endfor

font_coding_scheme "Malvern A " & maversion;
%}}}  Malvern A

% Local variables:
% fold-folded-p: t
% End:
