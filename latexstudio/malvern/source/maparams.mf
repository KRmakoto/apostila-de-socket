% maparams.mf 1.2.0 1994/10/11 -- Set ad-hoc parameters
% Copyright  1991-4 P. Damian Cugley.

%%% @METAFONT-file {
%%%   filename       = "maparams.mf",
%%%   version        = "1.2.0",
%%%   date           = "1994/10/11",
%%%   package        = "Malvern 1.2",
%%%   author         = "P. Damian Cugley",
%%%   email          = "damian.cugley@comlab.ox.ac.uk",
%%%   address        = "Oxford University Computing Laboratory,
%%%                     Parks Road, Oxford  OX1 3QD, UK",
%%%   codetable      = "USASCII",
%%%   keywords       = "Malvern, METAFONT, font, typefont, TeX",
%%%   supported      = "Maybe",
%%%   abstract       = "Define ad-hoc parameters for the Malvern
%%%                     font family.",
%%%   dependencies   = "other program files",
%%% }

%  See the Malvern Handbook (maman.tex) for more info about Malvern.
%  This software is available freely but without warranty.
%  See the file COPYING for details.

%{{{ maparams.mf

%{{{  meta-parameters (should be set before |\input ma|)

boolean italic;     % set if |italicness > 0| -- use italic letter-shapes

if unknown italicness: 
    italic := false;
    italicness := 0;
elseif italicness > 0:
    italic := true;
else:
    italic := false;
fi
% |italicness| is not referred to again

if unknown hratio: hratio := 1; fi

if unknown weight: weight := 1; fi

if unknown slant: slant := 0; fi
currenttransform := identity 
    if slant <> 0: slanted slant fi
    if aspect_ratio <> 1.0: yscaled aspect_ratio fi;

%  Warning message if selected obliqued font
if (slant <> 0) and (not italic):
    message "Please use italic instead of obliqued.";
    message "(Unless you know what you're doing!)";
fi
%}}}  meta-paramaters
%{{{  units of measurement
v# = 1/16 designsize;	    % unit of vertical measurement NB 1/16, not 1/18

u# = v# * hratio 
    if italic: * 0.95 fi; 	    % unit of horizontal measurement

sp# = 1.2u# 
    if designsize < 8pt#: * (8pt#/designsize) fi;  
			    % unit of inter-charcter spacing

ku# = 1/2[u#,sp#];	    % unit of kerning
%}}}  units
%{{{  heights and widths


body_ht# =		    % height of tallest characters
brack_ht# =		    % height of parentheses and other brackets
asc_ht# =		    % height of ascenders on l.c.\ letters like d
cap_ht# =		    % height of capital letters like X
fig_ht# = 12v#;		    % height of ranging figures
x_ht# = 8v#;		    % height of lower case  letters like x
axis_ht# = 5v#;		    % height of maths axis (rarely used)
brack_dp# = 2v#;	    % depth of parentheses etc.
ogonek_dp# = 2.5v#; 	    % depth of Polish ogonek
desc_dp# =		    % depth of descenders on letters like p
body_dp# = 4v#;		    % depth of deepest characters

%  |body_ht + body_dp = 16v = designsize|
%  Large x-height; no built-in leading

mcap_ht# = 7/8cap_ht#;	    % heigth of medium capitals
scap_ht# = 3/4cap_ht#;	    % height of small capitals
%  Adobe's favoured value is |1/3[x_ht#,cap_ht#]|

fig_wd#     = 8u# - 2sp#;			  % width of ranging figures
sfig_ht#    = 9v#;				  % height of superior figures
sfig_wd#    = 2sfig.pn.wd# + 3.75u#;		  % width of same

sup_x_ht# = 5.4v#;	% x-height of superior letters
sup_x_top# = 11v#;  	% superior letters raised so that x-height is here

dot_wd# = 1.25v# * weight;
dot_ht# = min(1.25v# * max(weight, 1), 3v#);

bullet_wd# = (weight - 1)[4v#,6v#]; % not affected by hratio!
bullet_ht# = (weight - 1)[4v#,6v#];

ho# = oo# = 1/5v#;	    % overshoot for letter o
o# = 2/3oo#;		    % overshoot for other curves
apex_o# = 1/2o#;	    % overshoot for apexes
apex_adjust# = 1/3u# * weight; % widen apexes by this much
%}}}
%{{{  pens

% wd = width   ht = height   th = thickness
% pn = pen (pen is a reserved word)

pn.wd#     	= if italic: 0.8v# else: 0.9v# fi 
    	    	    * weight; 	    	% the default pen width 

%  Lores hack:
if italic and (round(pn.wd# * hppp) < 0.75v# * weight * hppp): 
    pn.wd# := 0.9v# * weight; 
fi

comma.pn.th# 	= 4/5pn.wd#;
yen.pn.th# 	= min(3/4pn.wd#, v#); 	% bar in yen sign
ast.pn.th# 	= min(3/5pn.wd#, v#); 	% used in drawing asterisk 
dag.pn.th# 	= min(2/3pn.wd#, v#); 	% used in drawing dagger
acc.pn.th# 	= min(pn.wd#, v#);  	% used drawing marks

define_blacker_pixels(ast.pn.th, comma.pn.th, acc.pn.th);
define_whole_vertical_blacker_pixels(dag.pn.th, yen.pn.th);
pickup pencircle scaled acc.pn.th; mark_pn := savepen;
% the others are used once each so don't |savepn| them

cap.pn.wd# 	= pn.wd#;   %%%% 1.10*pn.wd#;	
mcap.pn.wd# 	= pn.wd#;   %%%% 1.05*pn.wd#;
scap.pn.wd# 	= 1.00*pn.wd#;
sfig.pn.wd#	= 0.90*pn.wd#;

%  ht = wd unless this would cause counters to fill up etc.
forsuffixes $$ = ,cap,mcap,scap:
    $$pn.ht# = min(2v#, $$pn.wd#);
endfor

sfig.pn.ht# = min((sfig_ht# - 2v#)/3, sfig.pn.wd#);

forsuffixes $$ = ,cap,mcap,scap,sfig:
    define_whole_blacker_pixels($$pn.wd);
    define_whole_vertical_blacker_pixels($$pn.ht);
endfor

%  lowres hacks!
if cap.pn.wd/pn.wd > 1.2:
    cap.pn.wd := mcap.pn.wd := scap.pn.wd := pn.wd;
    cap.pn.ht := mcap.pn.ht := scap.pn.ht := pn.ht;
fi

pickup pencircle xscaled pn.wd yscaled pn.ht;
the_pen = savepen;

%}}}
%{{{  usual sizes of marks

cap_mark_t# = body_ht# + 1.25v#;
cap_mark_b# = mcap_ht# + 0.75v#;
lc_mark_t# = body_ht#;
lc_mark_b# = x_ht# + v#;

%  assuming body_ht# = 12v#, mcap_ht# = 10v#, mcap marks go from
%  11v# to 13.5v# (2.5v#), so are flatter than l.c. marks.

acute_wd# = 3u#;
hook_wd# := pn.wd# + 1u#;	% hook for tall letters like "d" and "t"

%  These are all kept unrounded, and rounded later

cedilla_ht# = 3v#; cedilla_wd# = 3u#;
Cedilla_ht# = 4v#; Cedilla_wd# = 4u#;

%}}}
%{{{  define_pixels

define_pixels(sp,u,v);
define_whole_pixels(apex_adjust, acute_wd, hook_wd);
define_whole_vertical_pixels(brack_ht, body_ht, asc_ht, cap_ht, fig_ht, 
    mcap_ht, scap_ht, x_ht, desc_dp, body_dp, brack_dp, ogonek_dp,
    sup_x_ht, sup_x_top,
    sfig_ht, lc_mark_t, lc_mark_b, cap_mark_t, cap_mark_b);

define_whole_blacker_pixels(dot_wd, bullet_wd);
define_whole_vertical_blacker_pixels(dot_ht, bullet_ht);

define_good_y_pixels(axis_ht);

define_corrected_pixels(o, oo, apex_o);
define_horizontal_corrected_pixels(ho);

define_pixels(cedilla_ht, cedilla_wd, Cedilla_ht, Cedilla_wd);
%}}}
%{{{  Spacing of l.c. letters

a_sp = 0.6;
e_sp = o_sp;
f_lsp = 0.35; 
f_rsp = (0.5sp# - 2u#)/sp#;
i_sp = 1.0;
j_sp = (1sp# - 1.75u#)/sp#;
l_sp = 1.0;
n_sp = 0.9;
o_sp = 0.5;
r_sp = 0.4;
s_sp = 0.6;
v_sp = 0.4;
x_sp = 0.5;
%}}}
golden_ratio = 0.618034;
lc_mark_ht = lc_mark_t - lc_mark_b;
%}}} maparams.mf

%Local variables:
%fold-folded-p: t
%End:
