% malc.mf 1.2.0 1994/10/11 -- Lower case character programs
% Copyright  1991-4 P. Damian Cugley

%%% @METAFONT-file {
%%%   filename       = "malc.mf",
%%%   version        = "1.2.0",
%%%   date           = "1994/10/11",
%%%   package        = "Malvern 1.2",
%%%   author         = "P. Damian Cugley",
%%%   email          = "damian.cugley@comlab.ox.ac.uk",
%%%   address        = "Oxford University Computing Laboratory,
%%%                     Parks Road, Oxford  OX1 3QD, UK",
%%%   codetable      = "USASCII",
%%%   keywords       = "Malvern, METAFONT, font, typefont, TeX",
%%%   supported      = "Maybe",
%%%   abstract       = "Lower case latin character programs for the Malvern
%%%                     font family.",
%%%   dependencies   = "other program files",
%%% }

%  See the Malvern Handbook (maman.tex) for more info about Malvern.
%  This software is available freely but without warranty.
%  See the file COPYING for details.

%  Glyph programs for lower case letters that generate
%  both base letters and composite letters.
%  In some cases composite letters are generated in a separate
%  program from the base letter -- these go in malcco and malcnc.

%{{{ malc.mf

if testing: endinput; fi

%{{{  h

vardef draw_lc.h@#(expr l, by, r, h, bl, x_ht) = 
    top lft z@#stem1 = (l, h + o); bot lft z@#stem2 = (l, by - o);
    draw z@#stem1 -- z@#stem2; draw_n_arch@#(l,r); set_ic_n; 
enddef;

iff known code.lc.h:
b_char(code.lc.h, n_wd#)(1, n_sp); 
    "lower case h";
    draw_lc.h(l, -d, r, h, 0, x_ht);
endchar;
    
iff known code.lc.h.circumflex:
b_char(code.lc.h.circumflex, n_wd#)(1, n_sp); 
    "lower case h with circumflex";
    draw_lc.h(l, -d, r, h, 0, x_ht);
    draw_circumflex.silly(0.5[rt x.stem1, rt x4], lc_mark_b, 
    	min(x4 - x.stem1 - u, 2lc_mark_ht * hratio - pn.wd), lc_mark_ht) false;
    set_ic_tr;
endchar; 

%}}}  h
%{{{  w

vardef draw_lc.w@#(expr l, by, r, h, bl, x_ht) =
    if 1/2w <> apex.x 1/2w: change_width; fi;
    lft x@#1 = l; rt x@#5 = r; 
    x@#3 = 1/2[x@#1, x@#5]; 
    x@#2 = apex.x (1/2[x@#1,x@#3] - eps);
    x@#4 = apex.x (1/2[x@#3,x@#5] + eps);
    top y@#1 = top y@#5 = h + o;  top y@#3 = h; bot y@#2 = bot y@#4 = by;
    draw z@#1 -- vpex_path@#2 -- apex_path@#3 -- vpex_path@#4 -- z@#5;
    labels(@#1,@#2,@#3,@#4,@#5); set_ic_tr; x.anchor = 1/2[l,r];
enddef;

lc_with_composites.w(12u#, x_ht#, 0pt#)(v_sp, v_sp); 

%}}}  w
%{{{  t

vardef draw_lc.t@#(expr l, by, r, h, bl, x_ht) =
    top lft z@#1 = (hround (l + u), h + o); x@#2 = x@#1;
    bot rt z@#4 = (r, by); bot z@#3 = (min(4/5[l,r], x@#4 - eps), by); 
    z@#2 = z@#3 + whatever*se;
    draw z@#1 --- z@#2 .. z@#3 --- z@#4;
    lft x@#1bar = l; rt x@#2bar = x@#4; 
    top y@#2bar = top y@#1bar = x_ht;
    draw z@#1bar -- z@#2bar;
    labels(@#1,@#2,@#3,@#4,@#1bar,@#2bar); 
enddef;

iff known code.lc.t:
ma_char(code.lc.t, 
    	3u# + pn.wd#, 1/2[asc_ht#, x_ht#], 0pt#)(f_lsp, x_sp);
    draw_lc.t(l, 0, r, 1/2[asc_ht, x_ht], 0, x_ht);
    set_ic(x_ht#); charic := charic - 1/2pn.wd#;
endchar;

iff known code.lc.t.cedilla:
ma_char(code.lc.t.cedilla, 
    	3u# + pn.wd#, 1/2[asc_ht#, x_ht#], cedilla_ht#)(f_lsp, x_sp);
    draw_lc.t(l, 0, r, 1/2[asc_ht, x_ht], 0, x_ht);
    draw_cedilla.cedilla(1/2[l,r], 0, cedilla_wd, cedilla_ht);
    set_ic(x_ht#); charic := charic - 1/2pn.wd#;
endchar;

iff known code.lc.t.hook:
ma_char(code.lc.t.hook, 
    	3u# + pn.wd#, 1/2[asc_ht#, x_ht#], 0pt#)(f_lsp, x_sp);
    draw_lc.t(l, 0, r, 1/2[asc_ht, x_ht], 0, x_ht);
    draw_hook.hook;
    set_ic(x_ht#); charic := charic - 1/2pn.wd#;
endchar;

%}}}
%{{{  g

vardef draw_lc.g(expr l, by, r, h, bl, x_ht) =
    draw_b_bowl(l, bl, r, h)(bl, x_ht) false; 
    draw_tail1stem; set_ic_tr; x.anchor = x2bowl;
enddef;
lc_with_composites.g(b_wd#, x_ht#, desc_dp#)(o_sp, 1);

%}}}  g
%{{{  r

vardef draw_lc.r@#(expr l, by, r, h, bl, x_ht) =
    top lft z@#1stem = (l, h + o); bot lft z@#2stem = (l, by - o);
    z@#1arch = if italic: 1/3 else: 3/4 fi [z@#2stem, z@#1stem];
    top rt z@#2arch = (r, h + o);
    draw z@#1stem -- z@#2stem;
    draw z@#1arch{(z@#2arch - z@#1arch) yscaled 3} .. z@#2arch;
    labels(@#1arch, @#2arch); set_ic_tr; x.anchor = 0.4[l,r];
enddef;

lc_with_composites.r(4u#, x_ht#, 0pt#)(1, r_sp);

%}}}
%{{{  a
if not italic:
%{{{   roman a

vardef draw_lc.a@#(expr l, by, r, h, bl, x_ht) =
    lft x@#3bowl = l; rt x@#1bowl = rt x@#5bowl = r;
    bot y@#4bowl = by - o; top y@#2bowl = vround 2/3[by - o, h];
    x@#2bowl = x@#4bowl = 0.525[x@#3bowl, x@#1bowl];
    y@#1bowl = y@#2bowl - 3/4v; y@#5bowl = 1.5v + by - o; 
    y@#3bowl = 1/2[y@#2bowl, y@#4bowl];
    draw z@#1bowl .. z@#2bowl{left} .. z@#3bowl{down} 
    	.. z@#4bowl{right} .. z@#5bowl;
    labels(@#1bowl, @#2bowl, @#3bowl, @#4bowl, @#5bowl); 
    top y@#2spine = h + o; bot y@#4spine = by - o;
    rt x@#3spine = rt x@#4spine = r;
    x@#1spine = good.x 1/5[l,r]; x@#2spine = 0.575[x@#1spine, x@#3spine];
    z@#3spine = z@#2spine + whatever*se; 
    y@#1spine = good.y (min(y@#2spine, 
    	    	          max(y@#2spine - 0.75v, y@#2bowl + pn.ht + 1/2v)));
    draw z@#1spine .. z@#2spine{right} .. z@#3spine --- z@#4spine;
    labels(@#1spine, @#2spine, @#3spine, @#4spine);
    x.anchor = 0.585[l,r];
enddef;

a_wd# = 1/2pn.wd# + 6.5u#;

lc_with_composites.a(a_wd#, x_ht#, 0pt#)(a_sp, n_sp);

iff known code.lc.a.ring:
ma_char(code.lc.a.ring, a_wd#, lc_mark_t#, 0pt#)(a_sp, n_sp);
    "lower case a with ring";
    draw_lc.a(l, 0, r, x_ht, 0, x_ht);
    draw_clear_ring.ring(0.585[l,r], lc_mark_b, 3.5u, lc_mark_ht);
endchar;

iff known code.lc.a.ogonek:
ma_char(code.lc.a.ogonek, a_wd#, x_ht#, ogonek_dp#)(a_sp, n_sp);
    "lower case a with ogonek";
    draw_lc.a(l, 0, r, x_ht, 0, x_ht);
    pickup cedilla_pen(2.25u + 1/2pn.wd, ogonek_dp);
    x1ogonek = x4spine; y1ogonek = 0;
    y2ogonek = 0.5[y3ogonek, y1ogonek]; 
    lft x2ogonek = min(x4spine - 1.5u, lft x4spine - 1/2u);
    bot rt z3ogonek = (r + 0.75u, -ogonek_dp);
    draw z1ogonek{-ne} ... z2ogonek{down} ... z3ogonek{right};
    labels(1ogonek, 2ogonek, 3ogonek);
endchar;

iff known code.ord_feminine:
ma_char(code.ord_feminine, 3u# + 2pn.wd#, sup_x_top#, 0pt#)(0.5,0.5);
    draw_lc.a(l, vround (h - sup_x_ht), r, h, vround (h - sup_x_ht), h);
    pickup pencircle scaled yen.pn.th;
    top y1bar = top y2bar = bot y4spine - v;
    lft x1bar = l + 1/2u; rt x2bar = r - 1/4u;
    draw z1bar -- z2bar;
    labels(1bar,2bar); 
endchar;

%}}}   roman
else:
%{{{   italic a

vardef draw_lc.a(expr l, by, r, h, bl, x_ht) = 
    draw_b(l, by, r, h, bl, x_ht)false; set_ic_tr; x.anchor = 1/2[l,r];
enddef;

lc_with_composites.a(b_wd#, x_ht#, 0pt#)(o_sp, 1); 

iff known code.lc.a.ring:
ma_char(code.lc.a.ring, b_wd#, lc_mark_t#, 0pt#)(o_sp, 1); 
    "lower case a with ring";
    draw_lc.a(l, 0, r, x_ht, 0, x_ht);
    draw_clear_ring.ring(0.6[l,r], lc_mark_b, 3.5u, lc_mark_ht);
endchar;


iff known code.lc.a.ogonek:
ma_char(code.lc.a.ogonek, b_wd#, x_ht#, ogonek_dp#)(o_sp, 1); 
    "lower case a with ogonek";
    draw_lc.a(l, 0, r, x_ht, 0, x_ht);
    pickup cedilla_pen(3u, ogonek_dp);
    rt x3ogonek = r; 
    lft x2ogonek = hround (rt x3ogonek - 3u);
    bot y3ogonek = vround -ogonek_dp;
    top y1ogonek = 0; 
    rt x1ogonek = max(lft x1stem - 1/2u, rt x2ogonek);
    y2ogonek = max(y3ogonek, min(0.6[bot y3ogonek, top y1ogonek], y1ogonek));
    draw z1ogonek{left} ... z2ogonek{down} ... z3ogonek{right};
    labels(1ogonek, 2ogonek, 3ogonek);
    pickup the_pen;
endchar; 
    
iff known code.ord_feminine: "superior l.c. a";
ma_char(code.ord_feminine, 3.25u# + 2pn.wd#, sup_x_top#, 0pt#)(1/2,1/2);
    bot rt z1 = (r, vround (h - sup_x_ht));
    top rt z2 = (r, h);
    lft z3 = (l, 1/3[y1, y2]);
    bot z4 = (1/3[l,r], bot y1);
    rt z5 = (r, 1/4[y1, y2]);
    draw z1 -- z2{left} ... z3{down} ... z4{right} ... z5;
    labels(1, 2, 3, 4, 5);
    pickup pencircle scaled yen.pn.th;
    top y1bar = top y2bar = bot y1 - v;
    lft x1bar = l + 1/2u; rt x2bar = r - 1/4u;
    draw z1bar -- z2bar;
    labels(1bar,2bar); 
    set_ic_tr;
endchar;

%}}}   italic a
fi
%}}}  a
%{{{  u

vardef draw_lc.u(expr l, by, r, h, bl, x_ht) =
    draw_n_arch(r,l); draw (x1, y2) -- (x1, y4); set_ic_tr; 
    x.anchor = 1/2[l,r];
enddef;

lc_with_composites.u(n_wd#, x_ht#, 0pt#)(n_sp, 1);

iff known code.lc.u.ring:
ma_char(code.lc.u.ring,  n_wd#, lc_mark_t#, 0pt#)(n_sp ,1);
    "lower case u with ring!";
    draw_lc.u(l, -d, r, x_ht, 0, x_ht);
    draw_clear_ring.ring(0.5[l,r], lc_mark_b, 3.5u, lc_mark_ht);
endchar;

%}}}  u
%{{{  l
%{{{   versions with a tail

vardef draw_lc.lvar@#(expr l, by, r, h, bl, x_ht) =
    top lft z@#1 = (l, h + o); x@#2 = x@#1;
    bot z@#3 = (3/4[l,r], by); bot rt z@#4 = (r, by);
    z@#2 = z@#3 + whatever*se;
    draw z@#1 --- z@#2 .. z@#3 --- z@#4;
    labels(@#1,@#2,@#3,@#4);
enddef;

iff known code.lc.l1:
b_char(code.lc.l1, 2u# + pn.wd#)(1, -2/3);
    draw_lc.lvar(l, -d, r, h, 0, x_ht);
endchar;

iff known code.lc.l.slash1: "variant l.c. l with slash";
b_char(code.lc.l.slash1, 3u# + pn.wd#)(1/2, -1/2);
    draw_lc.lvar(l + u, -d, r, h, 0, x_ht);
    1/2[x1bar, x2bar] = x1; z2bar = z1bar + whatever*ne xscaled 2;
    lft x1bar = l; top y2bar = x_ht;
    draw z1bar -- z2bar; labels(1bar,2bar);
endchar;

iff known code.lc.l.acute1: "variant l.c. l with acute";
b_char(code.lc.l.acute1, 2u# + pn.wd#)(1/2, -1/2);
    draw_lc.lvar(l, -d, r, h - 2v, 0, x_ht);
    draw_acute_grave.acute(l + 1/2pn.wd, h - 1.5v, 
    	acute_wd, 1.5v) false;
endchar;

iff known code.lc.l.hook1: "variant l.c. l with hook";
b_char(code.lc.l.hook1, 2u# + pn.wd#)(1/2, -1/2);
    draw_lc.lvar(l, -d, r, h, 0, x_ht);
    draw_hook.hook;
endchar;


%}}}
%{{{   straight stick versions

iff known code.lc.l:
b_char(code.lc.l, pn.wd#)(l_sp, l_sp); 
    if 1/2w <> good.x 1/2w: change_width; fi
    draw (1/2w, bot (h + o)) -- (1/2w, top (-d - o)); set_ic_tr;
endchar;

iff known code.lc.l.slash: "l.c. l with slash";
b_char(code.lc.l.slash, pn.wd# + 2u#)(1/2,1/2); 
    if 1/2w <> good.x 1/2w: change_width; fi
    draw (1/2w, bot (h + o)) -- (1/2w, top (-d - o));
    1/2[x1bar, x2bar] = 1/2w; z2bar = z1bar + whatever*ne xscaled 2;
    lft x1bar = l; top y2bar = x_ht;
    draw z1bar -- z2bar;
    labels(1bar, 2bar); set_ic (1/2h#);
endchar;

iff known code.lc.l.hook: "lower-case l with hook";
b_char(code.lc.l.hook, pn.wd# + hook_wd#)(1,1/2);
    draw_hook;
    lft x1stem = lft x2stem = l;
    top y1stem = h + o; bot y2stem = -d - o;
    draw z1stem -- z2stem;
    labels(1stem, 2stem); set_ic_tr;
endchar;

iff known code.lc.l.acute: "lower-case l with acute mark";
b_char(code.lc.l.acute, max(pn.wd#, acute_wd# - sp#))(1,1);
    if 1/2w <> good.x 1/2w: change_width; fi    
    x1stem = x2stem = 1/2w; top y1stem = h - 2v; bot y2stem = -d - o;
    draw z1stem -- z2stem;
    labels(1stem, 2stem); set_ic_tr;
    draw_acute_grave.acute(1/2w, h - 1.5v, acute_wd, 1.5v) false;
endchar;

%}}}
%}}}
%{{{  c

def set_ic_frac expr s = set_ic (s * h#) enddef;
c_wd# = 6u# + pn.wd#;
c_k_kern# = c_h_kern# = 0pt#;
c_rsp = x_sp;

vardef draw_lc.c@#(expr l, by, r, h, bl, x_ht) =
    draw_C@#(l, h, r, by) 1/15; set_ic_frac 14/15; x.anchor = x2@#;
enddef;

lc_with_composites.c(c_wd#, x_ht#, 0pt#)(o_sp, c_rsp); 

iff known code.lc.c.cedilla:
ma_char(code.lc.c.cedilla, 6u# + pn.wd#, x_ht#, cedilla_ht#)(o_sp, x_sp); 
    "l.c. c with cedilla";
    draw_lc.c(l, 0, r, x_ht, 0, x_ht);
    draw_cedilla.cedilla(x2, 0, cedilla_wd, cedilla_ht);
endchar;

iff known code.sp.lc.c: "superior l.c. c";     % pdc Tue. 23 Apr. 1991
ma_char(code.sp.lc.c, 5u#, sup_x_top#, 0v#)(1/3,1/2);
    draw_C(l, h, r, vround (h - sup_x_ht)) 1/40;
    pickup pencircle scaled yen.pn.th;
    top y1bar = top y2bar = bot y4 - v;
    lft x1bar = l + 1/2u; rt x2bar = r - 1/4u;
    draw z1bar -- z2bar;
    labels(1bar,2bar); 
    set_ic_tr;
endchar;

%}}}  c
%{{{  s

def set_ic_lc_s =     set_icc(0.05[r#,l#], h# - v#) 1/3h# enddef;

vardef draw_lc.s@#(expr l, by, r, h, bl, x_ht) =
    draw_S(h, by)(0.52, 0.05, 0.05);
    set_ic_lc_s; x.anchor = 0.52[l,r];
enddef;

lc_with_composites.s(4u# + pn.wd#, x_ht#, 0pt#)(s_sp, s_sp); 

iff known code.lc.s.cedilla:
ma_char(code.lc.s.cedilla, 4u# + pn.wd#, x_ht#, cedilla_ht#)(s_sp, s_sp); 
    "l.c. s with cedilla";
    draw_lc.s(l, 0, r, x_ht, 0, x_ht);
    draw_cedilla.cedilla(1/2[l,r], 0, cedilla_wd, cedilla_ht);
endchar;

%}}}
%{{{  o 

o_wd# := if italic: 7u# else: 8u# fi;

vardef draw_lc.o(expr l, by, r, h, bl, x_ht) =
    draw_circle(l - ho, h + oo, r + ho, -d - oo); 
    set_ic_o; x.anchor = 1/2[l,r];
enddef;

lc_with_composites.o(o_wd#, x_ht#, 0pt#)(o_sp, o_sp);

iff known code.lc.o.cdot:
x_char(code.lc.o.cdot, o_wd#)(o_sp, o_sp); 
    "l.c. o with dot in centre";
    draw_lc.o(l, -d, r, h, 0, x_ht);
    z.dot = (x1, y0); draw_dot.dot;
endchar;

iff known code.ord_masculine:  "superior lower-case o";
ma_char(code.ord_masculine, sup_x_ht#*hratio, sup_x_top#, 0pt#)(1/2,1/2);
    draw_circle(l, h, r, h - sup_x_ht);
    set_ic_tr;
    pickup pencircle scaled yen.pn.th;
    top y1bar = top y2bar = bot y3 - v;
    lft x1bar = l + 1/2u; rt x2bar = r - 1/2u;
    draw z1bar -- z2bar;
    labels(1bar,2bar); 
endchar;

%}}}  o
%{{{  e
if italic:
%{{{   italic e

e_wd# := 6.5u# + 2ho#;

vardef draw_lc.e@#(expr l, by, r, h, bl, x_ht) =
    numeric rat; rat = 0.9;
    x@#4 = x@#2 = 0.55[l,r];    top y@#4 = h + o; bot y@#2 = by - o;% top & bot
    y@#3 = 1/2[y@#2,y@#4]; lft x@#3 = l;		  % left point
    y@#5 = 0.25[y@#4,y@#2]; rt x@#5 = r;		  % right point
    z@#6 = point rat of (z@#2{left} .. z@#3{up});	  % join
    y@#1 = y@#2 + 1/16(h - by); x@#1 = x@#5 - 1/13(r - l);
						  % terminal
    draw z@#1 .. z@#2{left} .. z@#3{up} .. z@#4{right} .. {down}z@#5 
%    	... z@#6{ (direction rat of (z@#2{left} .. z@#3{up})) rotated 90 };
    	... z@#6{left};
    x.anchor = x@#4;
    labels(1,2,3,4,5,6);
enddef;

lc_with_composites.e(e_wd#, x_ht#, 0pt#)(o_sp, e_sp);

iff known code.lc.e.ogonek:
ma_char(code.lc.e.ogonek, e_wd#, x_ht#, ogonek_dp#)(o_sp, e_sp);
    "lower case e with ogonek";
    draw_lc.e(l, 0, r, x_ht)(0, x_ht);
    numeric wd; wd = 2u + 1/2pn.wd;
    pickup cedilla_pen(wd + 1/2pn.wd, ogonek_dp);
    z1ogonek = z1;
    bot rt z3ogonek = (x1ogonek + wd, - ogonek_dp);
    z2ogonek = 0.75[(x3ogonek, y1ogonek), (x1ogonek, y3ogonek)];
    draw z1ogonek{down} ... z2ogonek{z3ogonek - z1ogonek} 
    	... z3ogonek{right};
    labels(1ogonek, 2ogonek, 3ogonek);
endchar;


vardef draw_lc.schwa@#(expr l, by, r, h, bl, x_ht) =
    numeric rat; rat = 0.9;
    x@#4 = x@#2 = 0.55[r,l];    bot y@#4 = by - o; top y@#2 = h + o;
						  % top & bot
    y@#3 = 1/2[y@#2,y@#4]; rt x@#3 = r;		  % left point
    y@#5 = 0.25[y@#4,y@#2]; lft x@#5 = l;	  % right point
    z@#6 = point rat of (z@#2{right} .. z@#3{down}); % join
    y@#1 = y@#2 + 1/16(by - h); x@#1 = x@#5 - 1/13(l - r);
						  % terminal
    draw z@#1 .. z@#2{right} .. z@#3{down} .. z@#4{left} .. {up}z@#5 
    	... z@#6{right};
    x.anchor = x@#4;
    labels(1,2,3,4,5,6);
enddef;

iff known code.lc.e.schwa: "lower case schwa";
x_char(code.lc.e.schwa, e_wd#)(e_sp, o_sp);
    draw_lc.schwa(l, 0, r, x_ht, 0, x_ht);
endchar;

%}}}   italic e
else:
%{{{   upright e

e_wd# = 7.5u# + 2ho#;

vardef draw_lc.e@#(expr l, by, r, h, bl, x_ht) =
    x@#1 = l + pn.wd; rt x@#2 = r; lft x@#4 = l; 
    top y@#3 = h + oo; bot y@#5 = by - oo; 
    y@#6 = good.y (y@#5 + 1/16(h - by)); x@#3 = x@#5 = 1/2[l, r]; 
    y@#1 = y@#2 = y@#4 = 0.525[by, h]; x@#6 = 0.825[l, r];
    draw z@#1 -- z@#2{up} .. z@#3 .. z@#4 .. z@#5{right} .. z@#6;
    labels(@#1,@#2,@#3,@#4,@#5,@#6); set_ic_e; x.anchor = x@#3;
enddef;

lc_with_composites.e(7.5u# + 2ho#, x_ht#, 0pt#)(o_sp, e_sp);

iff known code.lc.e.ogonek: "lower case e with ogonek";
ma_char(code.lc.e.ogonek, e_wd#, x_ht#, ogonek_dp#)(o_sp, e_sp);
    draw_lc.e(l, 0, r, x_ht, 0, x_ht);
    numeric wd; wd = 2u + 1/2pn.wd;
    pickup cedilla_pen(wd + 1/2pn.wd, ogonek_dp);
    z1ogonek = z5;
    bot rt z3ogonek = (x1ogonek + wd, - ogonek_dp);
    z2ogonek = 0.75[(x3ogonek, y1ogonek), (x1ogonek, y3ogonek)];
    draw z1ogonek{down} ... z2ogonek{z3ogonek - z1ogonek} 
    	... z3ogonek{right};
    labels(1ogonek, 2ogonek, 3ogonek);
endchar;


vardef draw_lc.schwa@#(expr l, by, r, h, bl, x_ht) =
    x@#1 = r - pn.wd; lft x@#2 = l; rt x@#4 = r; 
    bot y@#3 = by - oo; top y@#5 = h + oo; 
    y@#6 = good.y (y@#5 - 1/16(h - by)); x@#3 = x@#5 = 1/2[l, r]; 
    y@#1 = y@#2 = y@#4 = 0.525[h, by]; x@#6 = 0.825[r, l];
    draw z@#1 -- z@#2{down} .. z@#3 .. z@#4 .. z@#5{left} .. z@#6;
    labels(@#1,@#2,@#3,@#4,@#5,@#6); set_ic_e; x.anchor = x@#3;
enddef;

iff known code.lc.schwa:
x_char(code.lc.schwa, e_wd#)(e_sp, o_sp); "lower case schwa";
    draw_lc.schwa(l, 0, r, x_ht)(0, x_ht);
endchar;

%}}}   upright e
fi
%}}}
%{{{  n

vardef draw_lc.n@#(expr l, by, r, h, bl, x_wd) =
    top lft z@#1stem = (l, h + o);
    bot lft z@#2stem = (l, by - o);
    draw z@#1stem -- z@#2stem;
    draw_n_arch@#(l,r); set_ic_n; x.anchor=x@#2
enddef;

lc_with_composites.n(n_wd#, x_ht#, 0pt#, 1, n_sp);

%}}}
%{{{  y

vardef draw_lc.y@#(expr l, by, r, h, bl, x_ht) =
    draw_n_arch@#(r,l); top rt z@#0 = (r, h + o); draw_tail@#0; 
    set_ic_tr; x.anchor = 1/2[l,r];
enddef;

lc_with_composites.y(n_wd#, x_ht#, desc_dp#, n_sp, 1);

%}}}  y
%{{{  z

vardef draw_lc.z@#(expr l, by, r, h, bl, x_ht) =
    top lft z@#1 = (l, h); top rt z@#2 = (r, h); top z@#3 = z@#2;
    bot z@#4 = z@#5; bot lft z@#5 = (l, by); bot rt z@#6 = (r, by);
    draw z@#1 -- z@#2 -- z@#3 -- z@#4 -- z@#5 -- z@#6;
    labels(@#1, @#2, @#3, @#4, @#5, @#6); x.anchor = 1/2[l,r];
enddef;

lc_with_composites.z(7u#, x_ht#, 0pt#)(x_sp, x_sp);

%iff known code.lc.z.dot: "lower case z with dot";
%ma_char(code.lc.z.dot, 7u#, lc_mark_t#, 0pt#)(x_sp, x_sp);
%    draw_lc.z.z(l, 0, r, x_ht, 0, x_ht);
%    draw_dot_mark.dot_mark(1/2[l,r], lc_mark_b, 0, lc_mark_ht);
%endchar;

%}}}
%{{{  j

vardef draw_lc.j@#(expr l, by, r, h, bl, x_ht) =
    rt x@#1 = rt x@#2 = r; lft x@#3 = l; 
    top y@#1 = x_ht + o; bot y@#3 = by - o; bot y@#4 = good.y (bot y@#3 + 1v);
    z@#2 = z@#3 + whatever*ne;
    draw z@#1 --- z@#2 .. z@#3{left};
    labels(@#1,@#2,@#3); set_ic_tr; x.anchor = x@#1;
enddef;

%  There's a slight complication because the composites are
%  formed from a dotless j but are called j.foo not dotless_j.foo:

iff known code.lc.j:
ma_char(code.lc.j, 3u#, 
    	min(body_ht#, 1/2dot_ht# + 1/2[x_ht#, asc_ht#]), desc_dp#)
    	(j_sp,i_sp); 
    draw_lc.j(l, -d, r, h, 0, x_ht); top rt z.dot = (r, h); draw_dot.dot;
endchar;

iff known code.lc.dotless_j: "l.c. j without dot";
y_char(code.lc.dotless_j, 3u#)(j_sp,i_sp); 
    draw_lc.j(l, -d, r, h, 0, x_ht);
endchar;

lc_composites_only.j(3u#, x_ht#, desc_dp#)(j_sp, i_sp);

%}}}  j

input malcco
input malcnc
%{{{  ligatures

iff known code.lc.c.h:
ma_char(code.lc.c.h, c_wd# + n_wd# + c_h_kern# + sp#*(c_rsp + 1),
    asc_ht#, 0pt#)(o_sp, n_sp);
    "Ligature ch";
    draw_lc.c.c(l, 0, l + hround(c_wd# * hppp), x_ht, 0, x_ht);
    draw_lc.h.h(r - hround(n_wd# * hppp), 0, r, h, 0, x_ht);
endchar; 

iff known code.lc.c.k:
ma_char(code.lc.c.k, c_wd# + k_wd# + c_k_kern# + sp#*(c_rsp + 1),
    asc_ht#, 0pt#)(o_sp, x_sp);
    "Ligature ck";
    draw_lc.c.c(l, 0, l + hround(c_wd# * hppp), x_ht, 0, x_ht);
    draw_lc.k.k(r - hround(n_wd# * hppp), 0, r, h, 0, x_ht);
endchar; 

%}}}  ligatures
%}}} malc.mf

% Local variables:
% fold-folded-p: t
% End:
