# fmv.mff 1.2.0 1994/10/11 -- mff startup file for Malvern 1.2 with KB names

### @mff-options-file {
###   filename       = "fmv.mff",
###   version        = "1.2.0",
###   date           = "1994/10/11",
###   package        = "Malvern 1.2",
###   author         = "P. Damian Cugley",
###   email          = "damian.cugley@comlab.ox.ac.uk",
###   address        = "Oxford University Computing Laboratory,
###                     Parks Road, Oxford  OX1 3QD, UK",
###   codetable      = "USASCII",
###   keywords       = "Malvern, METAFONT, font, typefont, TeX",
###   supported      = "Maybe",
###   abstract       = "An alternative mff options file for the Malvern
###                     font family.",
###   dependencies   = "program files",
### }

#  See the Malvern Handbook (maman.tex) for more info about Malvern.
#  This software is available freely but without warranty.
#  See the file COPYING for details.

#  This file describes the translation of font names into assignments to
#  METAFONT variables (see the file INSTALL for more info).
#  It is used by programs like mff that run METAFONT automatically.

########################################################################
#
#  The font names understood are of the form
#
#    	fmv<weight><variant><encoding><width><size>
#
# where <weight> is one of t, i, l, k, m, d, b, x, c
#	<variant> is one of r, i, 9, 9i
#	<width> is one of c, r, x
#	<encoding> is one of k or <empty>
#	<size> is one or two digits
#
#  Except that if both <width> and <variant> are "r", we omit them.
#  Unless...  and so on.  The syntax is complicated.
#
#  The 7t suffix (indicating the old TeX Text encoding) is new.
#  It used to be that when there was no encoding suffix the assumption
#  was that "TeX Text" was being used.  It isn't supported yet!
#
#	font			old	   new		nonstandard name
#
#	Malvern 55 10-pt	fmvm10     fmvm7t10     ma55s10
#	Malvern 65 10-pt	fmvd10     fmvd7t10	ma65s10
#	Malvern 74 18-pt	fmvbix18   fmvbi7tx18   ma74s18
#	Malvern 58  7-pt	fmvmic7    fmvmi7tc7    ma58s7
#
########################################################################

-K	# tell mff that Karl Berry names are being used

#  The following sets the W table:

+W
-W "t= weight = 1/4"	# thin [ultra-light]
-W "i= weight = 1/2"	# extra-lIght
-W "l= weight = 3/4"	# Light
-W "k= weight = 7/8"	# booK
-W "m= weight = 1"	# Medium
-W "d= weight = 1.3" 	# Demi
-W "b= weight = 1.6"	# Bold
-W "x= weight = 2"	# eXtra
-W "u= weight = 3"	# Ultra

#  The table of widths (table C):

+C
-C "o= hratio = 0.50"	# extra condensed
-C "c= hratio = 0.80"	# compressed
-C "r= hratio = 1.00"	# normal width
-C "x= hratio = 1.15"	# expanded
-C "w= hratio = 1.30" 	# extra expanded

# table of variants:

+I
-I "i= italicness = 1; slant = 1/8"	# italic
-I "9= encoding = 26"		# oldstyle digits
-I "o= slant = 1/8"		# oblique
-I "u= italicness = 1"		# upright italic (!)
-I "r= "
-I "k= encoding = 7"		# Greek

#  The J table is not used:

+J
