% mamarks.mf 1.2.0 1994/10/11 -- marks program file
% Copyright  1991-4 P. Damian Cugley

%%% @METAFONT-file {
%%%   filename       = "mamarks.mf",
%%%   version        = "1.2.0",
%%%   date           = "1994/10/11",
%%%   package        = "Malvern 1.2",
%%%   author         = "P. Damian Cugley",
%%%   email          = "damian.cugley@comlab.ox.ac.uk",
%%%   address        = "Oxford University Computing Laboratory,
%%%                     Parks Road, Oxford  OX1 3QD, UK",
%%%   codetable      = "USASCII",
%%%   keywords       = "Malvern, METAFONT, font, typefont, TeX",
%%%   supported      = "Maybe",
%%%   abstract       = "Character programs for the Malvern
%%%                     font family -- stand-alone marks (accents).",
%%%   dependencies   = "other program files",
%%% }

%  See the Malvern Handbook (maman.tex) for more info about Malvern.
%  This software is available freely but without warranty.
%  See the file COPYING for details.

%{{{ mamarks.mf

%{{{  Set mark_t etc.

if lc_marks:
    mark_t# := lc_mark_t#; mark_b# := lc_mark_b#;
else:
    mark_t# := cap_mark_t# - (mcap_ht# - x_ht#); 
    mark_b# := cap_mark_b# - (mcap_ht# - x_ht#);
fi
define_whole_vertical_pixels(mark_t, mark_b);

def ma_mark(expr code) =
    ma_char(code, 8u#, mark_t#, 0pt#)(0,0);
    pickup mark_pn;
enddef;

%}}}


if testing: endinput; fi

%{{{  ring

"ring mark";
ma_mark(code.mk.ring);
    draw_circle(1/2w + -1.5u, mark_t, 1/2 w + 1.5u, 
    	mark_b if not lc_marks: - acc.pn.th + 2o fi);
endchar;

%}}}
%{{{  Grave, acute 

vardef draw_graveacute@#(expr leftx) =
    rtlft x2@# = hround leftx;
    lftrt x1@# = hround (rtlft x2@# minus 3u);
    x1'@# = x1@# plus (pn.wd - acc.pn.th);
    top y1@# = top y1'@# = mark_t; bot y2@# = mark_b;
    draw z2@# -- z1@# -- z1'@# -- z2@#;
    labels(1@#, 1'@#, 2@#, 3@#)
enddef;

def do_graveacute(expr code) expr grave_p =
    b := grave_p;
    ma_mark(code); draw_graveacute(1/2w plus u); endchar
enddef;

"grave mark"; do_graveacute(code.mk.grave) true;
"acute mark"; do_graveacute(code.mk.acute) false;

"Hungarian double acute mark";
ma_mark(code.mk.hungarian);
    draw_graveacute1(1/2w - if lc_marks: 2u else: 2.5u fi); 
    draw_graveacute2(1/2w + if lc_marks: u else: 0.5u fi);
endchar;
%}}}
%{{{  circumflex and inverted circumflex

def do_circumflex(expr code) expr inverted_p =
    b := inverted_p;
    ma_mark(code);
	x1 = w - x3 = good.x (1/2w - (if lc_marks: 3u else: 2.5u fi 
    	    	    	      - acc.pn.th + 1/2apex_adjust)); 
	x2 = x2' = 1/2[x1,x3];
	topbot y1 = topbot y3 = if inverted_p: mark_t else: mark_b fi; 
	bottop y2 = bottop y2' minus pn.wd plus acc.pn.th 
    	    = if inverted_p: mark_b else: mark_t fi;
	draw z1 -- z2 -- z3 -- z2' -- z1;
	labels(1,2,2',3);
    endchar;
enddef;

"circumflex mark"; do_circumflex(code.mk.circumflex) false;
"inverted circumflex mark"; do_circumflex(code.mk.hook) true;
%}}}
%{{{  breve

"breve mark";
ma_mark(code.mk.breve);
    top y1 = top y3 = mark_t; 
    bot y2 = mark_b; 
    top y2' = vround (y2 + (pn.wd - acc.pn.th) + 1/2acc.pn.th);
    lft x1 = w - rt x3 = hround (1/2w - 2.5u);
    x2 = x2' = 1/2[x1,x3];
    draw z1{down} .. z2{right} .. z3{up};
    % draw z1{down} .. z2'{right} .. z3{up};
    labels(1,2,2',3);
endchar;

%}}}
%{{{  tilde

"tilde";
ma_mark(code.mk.tilde);
    z3 = (1/2w, 1/2[mark_t, mark_b]);
    lft x1 = w - rt x5 = x3 - 3u;
    x2 = 1/2[x1,x3]; x4 = 1/2[x3,x5];
    top y2 = mark_t; bot y4 = mark_b;
    bot y1 = 0.75[mark_t, mark_b];
    top y5 = 0.75[mark_b, mark_t];
    save p; path p; p = z1 ... z2{right} ... z3 ... z4{right} ... z5;
    draw p;
%    penpos3(pn.wd - acc.pn.th, angle(direction 2 of p) + 90);
%    forsuffixes e=l,r: draw z2{right} ... z3e ... z4{right}; endfor
    labels(1, 2, 3l, 3, 3r, 5);
endchar;

%}}}

if lc_marks: 
  %{{{  dot, twodots

"two-dots mark";
ma_mark(code.mk.twodots);
    1/2[x1,x2] = 1/2w;
    y1 = y2 = 1/2[mark_t, mark_b];
    lft x2 = hround (1/2w - 1.5u - 1/2dot_wd);
    draw_dot1; draw_dot2;
    labels(1,2);
endchar;

"dot mark";
ma_mark(code.mk.dot)
    z1 = (1/2w, 1/2[mark_t, mark_b]);
    draw_dot1;
endchar;
%}}}
  %{{{  macrons
"macron";
ma_mark(code.mk.macron);
    1/2[x1,x2] = 1/2w; y1 = y2 = 1/2[mark_t, mark_b]; x2 - x1 = 4u;
    draw z1 -- z2; labels(1,2);
endchar;

if known code.mk.maacron: "macron for 2-letter vowels";
ma_mark(code.mk.maacron);
    1/2[x1,x2] = 1/2w; y1 = y2 = 1/2[mark_t, mark_b]; 
    x2 - x1 = 8u;
    draw z1 -- z2; labels(1,2);
endchar;
fi

%}}}
  if known code.mk.L_bar:
    %{{{  bar for l-bar

ma_char(code.mk.L_bar, 2u# + pn.wd#, 0.55cap_ht# + 1/2pn.ht#, 0pt#)(1/2,1)
    top lft z1 = (l, h); top rt z2 = (r, h);
    draw z1 -- z2;
endchar;

%ligtable code.mk.L_bar:
%    "l" kern -1sp# - l_sp*sp# - u# - pn.wd#,
%    "L" kern -2sp# - u# - pn.wd#;
%}}}  bar for l-bar
  fi
fi

%{{{  cedilla

ma_char(code.mk.cedilla, 
    if lc_marks: cedilla_wd#, 0pt#, cedilla_ht# 
    else: Cedilla_wd#, 0pt#, Cedilla_ht# fi)(1,1);
    pickup mark_pn;
    draw_cedilla(1/2w, 0, if lc_marks: cedilla_wd, cedilla_ht
    	    	    	  else: Cedilla_wd, Cedilla_ht fi);
endchar;

%}}}
%{{{  Cedilla and ogonek

iff known code.mk.ogonek: "ogonek mark";     
ma_char(code.mk.ogonek, 0u#, 0v#, 3v#)(0,0);
    pickup mark_pn;
    z1 = (good.x (1/2w + u), h); % overlaps baseline
    bot y3 - (pn.ht - acc.pn.th) = bot y2 = -d; 
    z2 = z1 + whatever * se; x3 = x2;
    draw z1{down} ... z2 -- z3 ... z1{up};
    labels(1,2,3);
endchar;

%%  This version is too complicated
%"cedilla";					  
%ma_char(code.mk.cedilla, 0u#, 0v#, 
%    	if lc_marks: 3/4 fi desc_dp#)(0,0);
%    pickup mark_pn;
%    x1 = x2 = x4 = good.x (1/2w if not lc_marks: + 1/2u fi);
%    y1 = h; % overlaps baseline
%    top y2 = h - if lc_marks: 2/3v else: v fi; 
%    bot y4 = -d; y3 = 1/2[y2,y4]; y5 = 1/6[y4, y2];
%    x5 = x1 - if lc_marks: 1.75u else: 2u fi; 
%    x3 = good.x (x1 + if lc_marks: 2u else: 2.5u fi);
%    draw z1 -- z2{right} .. z3{down} .. z4{left} .. z5;
%    z5' = z5 + (pn.wd - acc.pn.th)*up;
%    z4' = z4 + (pn.ht - acc.pn.th)*up;
%    z3' = z3 + (pn.ht - acc.pn.th)*left;
%    draw z2{right} .. z3'{down} .. z4'{left} .. z5';
%    labels(1,2,3,4,5,3',4',5');
%endchar;
%}}}

if known code.asper and lc_marks:
%{{{  Asper and Lenis (they don't belong here)

% pdc Wed. 20 Mar. 1991
for bb := true, false: b := bb;
    if b: "Greek asper, Arabic `ain, or Hebrew `ayin";
    else: "Greek lenis, Arabic hamza, or Hebrew 'aleph";
    fi
    ma_char(if b: code.asper else: code.lenis fi
	    if not lc_marks: + 128 fi, 
	    acc.pn.th# + 2u#, asc_ht#, 0v#)(0,0);
	pickup mark_pn;
	top y1 = mark_t;  y2 = min (y1, 0.4[mark_t, mark_b]);
	y1' = y1 - (pn.wd - acc.pn.th);
	y2' = y2 - (pn.ht - acc.pn.th);
	bot y3 = mark_b;
	lftrt x2 = lftrt x2' = lr; 
	rtlft x1 = rtlft x1' = rtlft x3 plus u = rl; 
	draw z3{(x2' - x3, 0)} ... z2'{up} ... {(x1 - x2, 0)}z1' 
	    -- z1{(x2 - x1, 0)} ... z2{down} ... {(x3 - x2, 0)}z3;
	labels(1,2,3,1');
    endchar;
endfor
%}}}
fi
%}}}

%Local variables:
%fold-folded-p: t
%End:
