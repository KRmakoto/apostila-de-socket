% macenc.mf 1.2.0 1994/10/11 -- Malvern encoding C (Cyrillic)
% Copyright 1994 P. Damian Cugley

%%% @METAFONT-file {
%%%   filename       = "macenc.mf",
%%%   version        = "1.2.0",
%%%   date           = "1994/10/11",
%%%   package        = "Malvern 1.2",
%%%   author         = "P. Damian Cugley",
%%%   email          = "damian.cugley@comlab.ox.ac.uk",
%%%   address        = "Oxford University Computing Laboratory,
%%%                     Parks Road, Oxford  OX1 3QD, UK",
%%%   codetable      = "USASCII",
%%%   keywords       = "Malvern, METAFONT, font, typefont, TeX",
%%%   supported      = "Maybe",
%%%   abstract       = "Encoding definition for the Malvern
%%%                     font family.",
%%%   dependencies   = "other program files",
%%% }

%  See the Malvern Handbook (maman.tex) for more info about Malvern.
%  This software is available freely but without warranty.
%  See the file COPYING for details.

%{{{ macenc.mf
%  no idea how to name the Cyrillic letters, sorry, so I am using the
%  transliterations in my dictionaries.
%  The encoding is based on the various *CYR10 fonts.
%  
%{{{  alphabets

code.cy.cap.nj 	    	= 0;		  % macedonian
code.cy.cap.lj 	    	= 1;		  % serbian
code.cy.cap.dz_hook    	= 2;		  % macedonian
code.cy.cap.e_acute    	= 3;
code.cy.cap.old_i  	= 4;		  % pre-1918 russian
code.cy.cap.ye 	    	= 5;		  % ukranian
code.cy.cap.d_bar    	= 6;		  % serbian
code.cy.cap.c_acute    	= 7;		  % serbian
code.cy.lc.nj  	    	= 8;		  % macedonian
code.cy.lc.lj  	    	= 9;		  % serbian
code.cy.lc.dz_hook    	= 10;		  % macedonian
code.cy.lc.e_acute    	= 11;
code.cy.lc.old_i   	= 12;		  % pre-1918 russian
code.cy.lc.ye  	    	= 13;		  % ukranian
code.cy.lc.d_bar	= 14;		  % serbian
code.cy.lc.c_acute	= 15;		  % serbian
code.cy.cap.yu 	    	= 16;
code.cy.cap.zh 	    	= 17;
code.cy.cap.i_breve    	= 18;		  % *
code.cy.cap.e_twodots  	= 19;		  % * obselete?
code.cy.cap.upsilon    	= 20;		  % * pre-1918, > i
code.cy.cap.theta    	= 21;		  % * pre-1918, > f
code.cy.cap.dz 	    	= 22;		  % macedonian
code.cy.cap.ya 	    	= 23;
code.cy.lc.yu  	    	= 24;
code.cy.lc.zh  	    	= 25;
code.cy.lc.i_breve	= 26;		  % *
code.cy.lc.e_twodots   	= 27;		  % * obselete?
code.cy.lc.upsilon	= 28;		  % * pre-1918, > i
code.cy.lc.theta    	= 29;		  % * pre-1918, > f
code.cy.lc.dz		= 30;		  % macedonian
code.cy.lc.ya  	    	= 31;
code.cy.cap.old_e    	= 35;		  % * pre-1918, > e
code.cy.lc.old_e    	= 43;		  % * pre-1918, > e
code.cy.cap.g_acute    	= 128;		  % * macedonian
code.cy.cap.k_acute    	= 129;		  % * macedonian
code.cy.cap.i_twodots  	= 130;		  % * ukranian
code.cy.cap.old_g    	= 131;		  % * ukranian, obselete
code.cy.cap.w  	    	= 132;		  % * belorussian
code.cy.lc.g_acute	= 136;		  % * macedonian
code.cy.lc.k_acute	= 137;		  % * macedonian
code.cy.lc.i_twodots   	= 138;		  % * ukranian
code.cy.lc.old_g	= 139;		  % * ukranian, obselete
code.cy.lc.w  	    	= 140;		  % * belorussian

code.cy.lc.a   	    	= ASCII"a";
code.cy.lc.b   	    	= ASCII"b";
code.cy.lc.ts   	= ASCII"c";		  % (like esperanto c!)
code.cy.lc.d   	    	= ASCII"d";
code.cy.lc.e   	    	= ASCII"e";
code.cy.lc.f   	    	= ASCII"f";
code.cy.lc.g   	    	= ASCII"g";
code.cy.lc.kh  	    	= ASCII"h";
code.cy.lc.i   	    	= ASCII"i";
code.cy.lc.j   	    	= ASCII"j";		  % serbian
code.cy.lc.k   	    	= ASCII"k";
code.cy.lc.l   	    	= ASCII"l";
code.cy.lc.m   	    	= ASCII"m";
code.cy.lc.n   	    	= ASCII"n";
code.cy.lc.o   	    	= ASCII"o";
code.cy.lc.p   	    	= ASCII"p";
code.cy.lc.ch  	    	= ASCII"q";
code.cy.lc.r   	    	= ASCII"r";
code.cy.lc.s   	    	= ASCII"s";
code.cy.lc.t   	    	= ASCII"t";
code.cy.lc.u   	    	= ASCII"u";
code.cy.lc.v   	    	= ASCII"v";  % actually 3rd letter of alfabet..
code.cy.lc.shch	    	= ASCII"w";
code.cy.lc.sh  	    	= ASCII"x";
code.cy.lc.y   	    	= ASCII"y";
code.cy.lc.z   	    	= ASCII"z"; % actually 9th letter of alphabet
code.cy.lc.soft_sign	= ASCII"~"; % '
code.cy.lc.hard_sign	= 127; % ''

code.cy.cap.a  	    	= ASCII"A";
code.cy.cap.b  	    	= ASCII"B";
code.cy.cap.ts  	= ASCII"C";		  % (like Esperanto c!)
code.cy.cap.d  	    	= ASCII"D";
code.cy.cap.e  	    	= ASCII"E";
code.cy.cap.f  	    	= ASCII"F";
code.cy.cap.g  	    	= ASCII"G";
code.cy.cap.kh 	    	= ASCII"H";
code.cy.cap.i  	    	= ASCII"I";
code.cy.cap.j  	    	= ASCII"J";		  % Serbian
code.cy.cap.k  	    	= ASCII"K";
code.cy.cap.l  	    	= ASCII"L";
code.cy.cap.m  	    	= ASCII"M";
code.cy.cap.n  	    	= ASCII"N";
code.cy.cap.o  	    	= ASCII"O";
code.cy.cap.p  	    	= ASCII"P";
code.cy.cap.ch 	    	= ASCII"Q";
code.cy.cap.r  	    	= ASCII"R";
code.cy.cap.s  	    	= ASCII"S";
code.cy.cap.t  	    	= ASCII"T";
code.cy.cap.u  	    	= ASCII"U";
code.cy.cap.v  	    	= ASCII"V";  % actually 3rd letter of alfabet..
code.cy.cap.shch    	= ASCII"W";
code.cy.cap.sh 	    	= ASCII"X";
code.cy.cap.y  	    	= ASCII"Y";
code.cy.cap.z  	    	= ASCII"Z"; % actually 9th letter of alphabet
code.cy.cap.soft_sign  	= ASCII"^"; % '
code.cy.cap.hard_sign  	= ASCII"_"; % ''
%}}}  alphabets
%{{{  substitutions2

%  Substitute similar latin or greek letters.  Some letters like
%  cy.cap.k, cy.cap.r, cy.cap.u are normally drawn slightly differently
%  from their latin sijmilars cap.k, cap.p, cap.y, but these will
%  do for now.

vardef subst(suffix $, $$) = 
    if unknown code$: code$ := code.cy$$; fi
enddef;

% forsuffixes $=y,j: subst(cap$,cap$); endfor
forsuffixes $=e,y,j: subst(lc$,lc$); endfor

% subst(cap.c, cap.s);
% subst(cap.s, cap.dz);
% subst(cap.v, cap.upsilon);			  % not a great match
subst(cap.e.twodots, cap.e_twodots);
subst(gr.cap.alpha, cap.a);    
subst(gr.cap.beta, cap.v);
subst(gr.cap.chi, cap.kh);
subst(gr.cap.epsilon, cap.e);
subst(gr.cap.eta, cap.n);
subst(gr.cap.gamma, cap.g);
subst(gr.cap.iota, cap.old_i);
subst(gr.cap.kappa, cap.k);
subst(gr.cap.mu, cap.m);
subst(gr.cap.omicron, cap.o);
subst(gr.cap.phi, cap.f);
subst(gr.cap.rho, cap.r);
 
subst(lc.a, lc.a);
subst(sc.b, lc.v);
if italic: subst(lc.eth, lc.d); fi
subst(lc.e, lc.e);
subst(lc.e.twodots, lc.e_twodots);
subst(lc.sc.h, lc.n);
subst(gr.lc.omicron, lc.o);
if italic: subst(lc.n, lc.p); fi
subst(gr.lc.rho, lc.r);
subst(lc.c, lc.s);
if italic: subst(lc.m, lc.t); fi
subst(lc.y, lc.u);
subst(gr.lc.phi, lc.f);
subst(lc.x, lc.kh);
subst(lc.i, lc.old_i);
subst(lc.v, lc.upsilon);
subst(lc.y.breve, lc.w);
subst(lc.j, lc.j);
subst(lc.i.twodots, lc.i_twodots);


%}}}  substitutions
%{{{  digits, punctuation, symbols

code.mk.twodots	    	= ASCII" ";
code.exclam 		= ASCII"!";
code.dbl.apostrophe 	= ASCII ditto;
code.mk.breve	    	= ASCII"$";
code.percent		= ASCII"%";
code.mk.acute	    	= ASCII"&";
code.apostrophe		= ASCII"'";
code.paren.left		= ASCII"(";
code.paren.right	= ASCII")";
code.asterisk6	    	= ASCII"*";
code.comma  	    	= ASCII",";
code.hyphen		= ASCII"-";
code.full_stop		= ASCII".";
code.slash		= ASCII"/";
code.zero   	    	= ASCII"0";
code.colon		= ASCII":";
code.semicolon		= ASCII";";
code.guillemet.left	= ASCII"<";
code.lc.dotless_i   	= ASCII "=";
code.guillemet.right	= ASCII">";
code.question	    	= ASCII"?";
code.mk.breve1	    	= ASCII"@";
code.brack.left   	= ASCII"[";
code.brack.right  	= ASCII"]";
code.dbl.inv.comma  	= ASCII"\";
code.inv.comma	    	= ASCII"`";
code.en_dash	    	= 121;
code.em_dash	    	= 122;
code.numero 	    	= 123;

%}}}  digits, punctuation, symbols
font_coding_scheme "Malvern C " & maversion;

%}}}

% Local variables:
% fold-folded-p: t
% End:
