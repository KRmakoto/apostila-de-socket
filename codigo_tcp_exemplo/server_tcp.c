/***********************************************************/
/*             CODIGO SERVIDOR TCP - sockaddr_in           */
/*                                                         */
/*  Codigo exemplo do servidor atraves do protocolo TCP com*/
/* a estrutura sockaddr_in.                                */
/*                                                         */
/*  Esta e a versao completa, com estruturas condicionais e*/
/* funcao para trabalhar com erro, o codigo esta destinado */
/* ao anexo da apostila.                                    */
/***********************************************************/

/*
 * STD - CLIB
 */
#include <stdio.h>     //I/O Padrao
#include <errno.h>     //Biblioteca de erro :errno
#include <stdlib.h>    //EXIT_ SUCESS/FAILURE, memset()
#include <string.h>    //Manipular String

/*
 * GLIBC & POSIX
 */
#include <netdb.h>      //Traducao proto. para end. num.
#include <unistd.h>     //Var. e const. para func. POSIX
#include <sys/types.h>  //Diversos tipos de dados
#include <netinet/in.h> //Proto. de inet e familia de end.
                        //in.h >> Possuí <sys/socket.h>


/*
 * DEFINE
 */
#define PORT    50505   //Porta de rede do servidor
#define BACKLOG 5       //Numero maximo da fila de espera



/*
 * FUNCOES
 */

 /*Imprime o Erro do Errno*/
void getError(char *txt)
  {
  fprintf
    (
    stderr,                 //Saida de erro padrao >> 2
    "%s\n Erro num: %d - %s\n", txt,
    errno, strerror(errno) //Errno e um valor int e precisa do
    );                      //strerror para identificar o erro

  fflush(stdout);
  }



/*
 * MAIN
 */
int
main(int argc, char *argv[])
{
   /*
    * Variaveis
    */
  char
    send_msg[20];

  int
    sockfd,        //Descritor de arquivo do soquete
    bind_r,        //Retorno da sys call bind
    listen_r,      //Retorno da sys call listen
    accept_r,      //Retorno da sys call accept
    client_num = 0;

  ssize_t   //Definicao de inteiro
    send_len;      //Numero de bytes enviados

  socklen_t //Definicao de inteiro
    server_len,    //Tamanho do endereco do servidor
    client_len;    //Tamanho do endereco do cliente

  struct sockaddr_in 
    serveraddr_in, //Endereco de soquete IPv4 do servidor
    clientaddr_in; //Endereco de soquete IPv4 do cliente
   


   /*
    * Inicializacao
    */
  memset( &serveraddr_in, 0, sizeof( struct sockaddr ) );

  serveraddr_in.sin_family = AF_INET;
  serveraddr_in.sin_port   = htons(PORT);
  serveraddr_in.sin_addr.s_addr = INADDR_ANY;
 
  server_len = sizeof(serveraddr_in);
  client_len = sizeof(clientaddr_in);


   /*
    * Execucao
    */
   /*SOCKET - Criando um SOQUETE TCP */
  sockfd = socket
    (
    PF_INET,      //Para IPv4 apenas
    SOCK_STREAM,  //Familia de Protocolo TCP
    0             //Zero para comportamento padrao
    );

   //Verificando se foi criado com sucesso
  if( sockfd == -1 )
  {
    getError("Funcao Socket");
    exit(EXIT_FAILURE);
  }


   /*BIND - Associando endereco com soquete */
  bind_r = bind
    (
    sockfd,                            //FD do socket TCP criado
    (struct sockaddr *) &serveraddr_in,//Cast, addr_in p/ addr. 
    server_len                         //Tamanho do endereco
    );

   //Verificando houve erro na associacao
  if( bind_r == -1 )
  {
    getError("Funcao Bind");
    exit(EXIT_FAILURE);
  }


   /*LISTEN - Preparando para receber conexao */
  listen_r = listen
    (
    sockfd,  //FD do soquete TCP criado
    BACKLOG  //Const. definida para num. max. na fila de espera
    );
   //Se nao foi criada a fila de espera
  if( listen_r == -1 )
  {
    getError("Funcao Listen");
    exit(EXIT_FAILURE);
  }

  while(client_num < 10)
  {
     //Limpa a estrutura do cliente
    memset( &clientaddr_in, 0, sizeof( struct sockaddr ) );

     /*ACCEPT - Segmenta em uma nova conexao */
    accept_r = accept
      (sockfd, (struct sockaddr *)&clientaddr_in, &client_len);

     //Se houve erro ao estabelecer conexao
    if( accept_r == -1 )
    {
       getError("Funcao Accept");
       continue;
    }

     //Imprime no terminal o descritor do accept
    printf("Accp fd %d\n", accept_r);

     /*SEND - Envia uma mensagem para o cliente*/
     //Preparado o vetor de char para enviar
    sprintf(send_msg, "Cliente numero %d", ++client_num);

     //Avisa o tamanho e qual mensagem sera enviada
    printf("  Tamanho msg : %d - msg :%s\n\n",
           strlen(send_msg), send_msg);
     //Forca o buffer de saida a escrever no terminal
    fflush(stdout);

     //Envia a mensagem para o cliente
    send_len = send
      ( 
      accept_r,        //Novo sock para conexao com cliente
      send_msg,        //Mensagem a ser enviada
      strlen(send_msg),//Tamanho da mensagem
      0                //Se 0, comportamento igual a write()
      );

     //Verifica se houve erro na mensagem enviada
    if( send_len == -1 )
    {
      getError("Funcao Send");
    }

     /*Encerra o descritor do accept a cada volta*/
     //Se retirar serao gerados varios descritores
    close(accept_r);
  }

   /*Encerra o descritor do soquete*/
  close( sockfd ); 

  return(EXIT_SUCCESS);
}

