#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>      //Traducao proto. para end. num.
#include <unistd.h>     //Var. e const. para func. POSIX
#include <sys/types.h>  //Diversos tipos de dados
#include <netinet/in.h> //Proto. de inet e familia de end.
                        //in.h >> Possuí <sys/socket.h>

/* DEFINE */
#define PORT    50505
#define BACKLOG 5


/* MAIN */
int main(int argc, char *argv[])
{
   /* Variaveis --------------------------------------------------------------*/
  char 
    recv_msg[20];

  int
    sockfd,        //Descritor de arquivo do soquete
    conn_r;        //Retorno da funcao de conexao

  ssize_t
    recv_len;      //Num. de bytes recebidos

  socklen_t 
    server_len;    //Tamanho do end. do servidor

  struct sockaddr_in 
    serveraddr_in; //End. de soquete IPv4 do servidor
   

   /* Inicializacao */
  memset( &serveraddr_in, 0, sizeof( struct sockaddr ) );

   //Apenas para teste, evite usar este formato
  serveraddr_in.sin_family = AF_INET;
  serveraddr_in.sin_port = htons(PORT);
  serveraddr_in.sin_addr.s_addr = inet_addr("127.0.0.1");
  server_len = sizeof(serveraddr_in);


   /* Execucao ---------------------------------------------------------------*/
   /*SOCKET - Criando um SOQUETE TCP */
  sockfd = socket( PF_INET, SOCK_STREAM, 0 );

   /*CONNECT - Estabelecendo conexao */
  conn_r = connect( sockfd, (struct sockaddr *) &serveraddr_in, server_len );

   /*SEND - Envia uma mensagem para o cliente*/
  recv_len = recv( sockfd, recv_msg, 19, 0 );
   //Adiciona 0x00 no final
  recv_msg[recv_len] = '\0';
  printf("SIZE: %d - BY SERVER: %s\n",strlen(recv_msg), recv_msg);

  /* Encerramento*/
  close( sockfd );
  return(EXIT_SUCCESS);
}
