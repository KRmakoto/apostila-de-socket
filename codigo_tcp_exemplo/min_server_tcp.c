#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>      //Traducao protocolos para end. numericos
#include <unistd.h>     //Variareis e constantes para func. POSIX
#include <sys/types.h>  //Diversos tipos de dados
#include <netinet/in.h> //Proto. de internet e familia de end.
                        //ih.h >> Possuí <sys/socket.h>

/* DEFINE */
#define PORT    50505
#define BACKLOG 5


/* MAIN ----------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
   /* Variaveis */
  char
    send_msg[20];

  int
    sockfd,        //Descritor de arquivo do soquete
    bind_r,        //Retorno da sys call bind
    listen_r,      //Retorno da sys call listen
    accept_r,      //Retorno da sys call accept
    client_num = 0;

  ssize_t   //Definicao de inteiro
    send_len,      //Numero de bytes enviados
    recv_len;      //Numero de bytes recebidos

  socklen_t //Definicao de inteiro
    server_len,    //Tamanho do endereco do servidor
    client_len;    //Tamanho do endereco do cliente

  struct sockaddr_in 
    serveraddr_in, //Endereco de soquete IPv4 do servidor
    clientaddr_in; //Endereco de soquete IPv4 do cliente
   

   /* Inicializacao ----------------------------------------------------------*/
  memset( &serveraddr_in, 0, sizeof( struct sockaddr ) );

  serveraddr_in.sin_family = AF_INET;
  serveraddr_in.sin_port   = htons(PORT);
  serveraddr_in.sin_addr.s_addr = INADDR_ANY;
  server_len = sizeof(serveraddr_in);
  client_len = sizeof(clientaddr_in);

   /* Execucao */
   /*SOCKET - Criando um SOQUETE TCP */
  sockfd = socket
    (
    PF_INET,      //Para IPv4 apenas
    SOCK_STREAM,  //Familia de Protocolo TCP
    0             //Zero para comportamento padrao
    );

   /*BIND - Associando endereco com soquete */
  bind_r = bind
    (
    sockfd,                             //FD do soquete TCP criado
    (struct sockaddr *) &serveraddr_in, //Cast, addr_in para addr 
    server_len                          //Tamanho do endereco>
    );

   /*LISTEN - Preparando para receber conexao */
  listen_r = listen
    (
    sockfd,  //FD do soquete TCP criado
    BACKLOG  //Const. definida para num. max. na fila de espera
    );

  while("happy")
  {
     //Limpa a estrutura do cliente
    memset( &clientaddr_in, 0, sizeof( struct sockaddr ) );

     /*ACCEPT - Segmenta em uma nova conexao */
    accept_r = accept( sockfd, (struct sockaddr *)&clientaddr_in, &client_len );

    printf("Accp fd %d\n", accept_r);

     /*SEND - Envia uma mensagem para o cliente*/
    sprintf(send_msg, "Cliente numero %d", ++client_num);
    printf("  Tamanho msg : %d - msg :%s\n\n", strlen(send_msg), send_msg);
    fflush(stdout);

    send_len = send
      ( 
      accept_r,         //Novo soquete criado para conexao com cliente
      send_msg,         //Mensagem a ser enviada
      strlen(send_msg), //Tamanho da mensagem
      0                 //Se zero, comportamento igual a write()
      );
    /*Encerra o descritor do accept*/
    close(accept_r);
  }
  /* Encerramento ------------------------------------------------------------*/
  close(sockfd);
  return(EXIT_SUCCESS);
}

