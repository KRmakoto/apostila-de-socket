/***********************************************************/
/*              CODIGO CLIENTE TCP - sockaddr_in           */
/*                                                         */
/*  Codigo exemplo do cliente atraves do protocolo TCP com */
/* a estrutura sockaddr_in.                                */
/*                                                         */
/*  Esta e a versao completa, com estruturas condicionais e*/
/* funcao para trabalhar com erro, o codigo esta destinado */
/* ao anexo da apostila.                                   */
/***********************************************************/

/*
 * STD - CLIB
 */
#include <stdio.h>     //I/O Padrao
#include <errno.h>     //Biblioteca de erro :errno
#include <stdlib.h>    //EXIT_ SUCESS/FAILURE, memset()
#include <string.h>    //Manipular String

/*
 * GLIBC & POSIX
 */
#include <netdb.h>      //Traducao proto. para end. num.
#include <unistd.h>     //Var. e const. para func. POSIX
#include <sys/types.h>  //Diversos tipos de dados
#include <netinet/in.h> //Proto. de inet e familia de end.
                        //in.h >> Possuí <sys/socket.h>


/*
 * DEFINE
 */
#define PORT    50505   //Porta de rede do servidor
#define BACKLOG 5       //Numero maximo da fila de espera



/*
 * FUNCOES
 */

 /*Imprime o Erro do Errno*/
void getError(char *txt)
  {
  fprintf
    (
    stderr,                 //Saida de erro padrao >> 2
    "%s\n Erro num: %d - %s\n", txt,
    errno, strerror(errno) //Errno e um valor int e precisa do
    );                      //strerror para identificar o erro
  }



/*
 * MAIN
 */
int
main(int argc, char *argv[])
{
   /*
    * Variaveis
    */
  char
    recv_msg[20];

  int
    sockfd,        //Descritor de arquivo do soquete
    conn_r;        //Retorno da funcao connect()

  ssize_t   //Definicao de inteiro
    recv_len;      //Numero de bytes recebidos

  socklen_t //Definicao de inteiro
    server_len;    //Tamanho do endereco do servidor

  struct sockaddr_in 
    serveraddr_in; //Endereco de soquete IPv4 do servidor
   

   /*
    * Inicializacao
    */
  memset( &serveraddr_in, 0, sizeof( struct sockaddr ) );

   //Apenas para teste, evite usar este formato
  serveraddr_in.sin_family = AF_INET;
  serveraddr_in.sin_port = htons(PORT);
  serveraddr_in.sin_addr.s_addr = inet_addr("127.0.0.1");
 
  server_len = sizeof(serveraddr_in);


   /*
    * Execucao
    */

   /*SOCKET - Criando um SOQUETE TCP */
  sockfd = socket( PF_INET, SOCK_STREAM, 0 );

   //Verificando se foi criado com sucesso
  if( sockfd == -1 )
  {
    getError("Funcao Socket");
    return(EXIT_FAILURE);
  }


   /*CONNECT - Estabelecendo conexao */
  conn_r = connect
    (sockfd, (struct sockaddr *) &serveraddr_in, server_len);

   //Se houver erro ao tentar conexao com servidor
  if( conn_r == -1 )
  {
     getError("Funcao Connect");
     return(EXIT_FAILURE);
  }


   /*RECV - Recebe a mensagem do servidor*/
  recv_len = recv(sockfd, recv_msg, 19, 0);

   //Verifica se nao recebeu errado os bytes
  if( recv_len == -1 )
  {
    getError("Funcao Send");
  }

   //Adiciona \0 no final do vetor
  recv_msg[recv_len] = '\0';
   //Imprime na tela a mensagem recebida
  printf("SIZE: %d - BY SERVER: %s\n",
         strlen(recv_msg), recv_msg);

   /*Encerra o descritor do soquete*/
  close( sockfd );

  return(EXIT_SUCCESS);
}
